# Copyright [2014]. California Institute of Technology.  ALL RIGHTS RESERVED.
# U.S. Government sponsorship acknowledged. Any commercial use must be
# negotiated with the Office of Technology Transfer at the California
# Institute of Technology.

# This software is subject to U. S. export control laws and regulations
# (22 C.F.R. 120-130 and 15 C.F.R. 730-774). To the extent that the
# software is subject to U.S. export control laws and regulations, the
# recipient has the responsibility to obtain export licenses or other
# export authority as may be required before exporting such information
# to foreign countries or providing access to foreign nationals.

import warnings
from cmath import *
import math
import numbers
import collections

import numpy as np
import scipy.signal 

############################################################################
# Class: Hex Array
#
# An implementation of ASA addressed Hexagonal Planar Arrays using
# numpy.ndarray under the hood.
############################################################################
class Array(object):
  def __init__(self, shape, dtype=np.double, a0=0, alloc=True):
    self.array = None
    self.h     = None
    self.w     = None
    self.a0    = a0
    self.shape = shape
    self.dtype = dtype
    self.fullArrayHeight = None
    self.subArrayHeight  = None
    self.rebase2dSlices  = False

    # Compute and store relevant bookkeeping info
    (h,w) = shape
    subHeight = -(-(h+a0)/2)
    self.h = h
    self.w = w

    self.fullArrayHeight = subHeight * 2
    self.subArrayHeight  = subHeight

    # Create and initialize the array
    if alloc:
      array      = np.empty((2,subHeight,w), dtype=dtype)
      array[:]   = np.NAN
      self.array = array

  @property
  def as2d(self):
    a = self.array.transpose((1,0,2)).reshape((-1,self.w))
    return a[self.a0:self.h+self.a0,:]

  def __str__(self):
    return self.array.__str__()

  def __repr__(self):
    t = type(self)
    mod = t.__module__
    name = t.__name__
    return ("%s.%s(a0=%d, h=%d, w=%d\n%s\n)" %
            (mod, name, self.a0, self.h, self.w, self.array.__repr__()))

  def __len__(self):
    return (h*w)

  def __getitem__(self, key):
    try:
      key = tuple(key)
      (a,r,c) = key
      if (a < 0) or (a > 1):
        raise IndexError("index %d out of range (0<=index<2) in 'a'" % a)
      if (r < 0) or (r >= self.subArrayHeight):
        raise IndexError("index %d out of range (0<=index<%d) in 'r'" %
                         (r, self.subArrayHeight))
      if (c < 0) or (c >= self.w):
        raise IndexError("index %d out of range (0<=index<%d) in 'c'" %
                         (c, self.w))
    except:
      if isinstance(key, slice):
        pass
      elif isinstance(key, tuple):
        if len(key) == 3:
          return array(self.array[key])
        if len(key) == 2:
          return self.get2dSlice(key)
      else:
        raise TypeError("key is not coercable to type 'AsaCoord'")

    return self.array[key]

  def get2dSlice(self,key,rebase=False):
    (rObj, c) = key
    a0 = self.a0
    if isinstance(rObj, slice):
      (start, stop, step) = (rObj.start, rObj.stop, rObj.step)
      if rObj.step is None:
        step  = 1
      if rObj.start is None:
        start = 0
      if rObj.stop is None:
        stop  = self.h
      start += a0
      stop  += a0
      r0 = start/2
      rF = stop/2
      r0off = start%2
      rFoff = stop%2
      if isinstance(c, slice):
        (c0, cF, cS) = (c.start, c.stop, c.step)
        if c.start is None:
          c0 = 0
        if c.stop is None:
          cF = self.w
        c_w = cF - c0
      else:
        c0 = c
        cF = c+1
        c_w = 1
      # If we don't need or aren't requesting rebasing...
      if (r0off == 0 or not (self.rebase2dSlices or rebase)):  
        a = self.array[:,r0:rF+rFoff,c0:cF].transpose((1,0,2)).reshape((-1,c_w))
        return array(a[r0off:a.shape[0]-rFoff,:], dtype=self.dtype, a0=r0off)
      # Looks like we need to pull the big guns and rebase the starting
      # row from a0=1 to a0=0.
      else:
        a = np.array([self.array[r%2,r/2,(c0+(r+1)%2):(cF+(r+1)%2)]
                      for r in xrange(start,stop)], dtype=self.dtype)
        return array(a, dtype=self.dtype)
    else:
      r = rObj + a0
      return self.array[r%2,r/2,c]

  def __setitem__(self, key, value):
    try:
      key = tuple(key)
      (a,r,c) = key
      if (a < 0) or (a > 1):
        raise IndexError("index %d out of range (0<=index<2) in 'a'" % a)
      if (r < 0) or (r >= self.subArrayHeight):
        raise IndexError("index %d out of range (0<=index<%d) in 'r'" %
                         (r, self.subArrayHeight))
      if (c < 0) or (c >= self.w):
        raise IndexError("index %d out of range (0<=index<%d) in 'c'" %
                         (c, self.w))
    except:
      if isinstance(key, slice):
        pass
      elif (isinstance(key, tuple) and
            len(key) == 3):
        pass
      else:
        raise TypeError("key is not coercable to type 'AsaCoord'")
    
    self.array[key] = value

    
    
############################################################################
# Array Support Functions
############################################################################
def empty(size, dtype=np.double, a0 = 0, alloc=True):
  return Array(size, dtype, a0=a0, alloc=alloc)

def zeros(size, dtype=np.double, a0 = 0):
  a = Array(size, dtype, a0=a0)
  a.array[:] = 0
  return a

def ones(size, dtype=np.double, a0 = 0):
  a = Array(size, dtype, a0=a0)
  a.array[:] = 1
  return a

def array(value, dtype=None, a0 = 0):
  if isinstance(value, np.ndarray):
    if dtype is None:
      dtype = value.dtype      
    dims = len(value.shape)
    if dims == 2:
      (h,w) = value.shape
      ha = -(-(h+a0)/2) * 2
      if h != ha:
        val = np.empty((ha,w),dtype=dtype)
        val[:] = np.NAN
        val[a0:(h+a0),:] = value[0:h,:]
      else:
        val = value
      
      a = empty((h, w), dtype=dtype, a0=a0, alloc=False)
      a.array = val.reshape((ha/2, 2, w)).transpose((1,0,2))[:]
    elif dims == 3:
      (ax, h2, w) = value.shape
      
      a = empty((2*h2, w), dtype=dtype, a0=a0)
      a[:] = value[:]
    else:
      raise TypeError("argument must be an ndarray of 2 or 3 dims")
  elif isinstance(value, Array):
    if dtype is None:
      dtype = value.dtype
    a = empty(value.shape, dtype=dtype)
    a[:] = value[:]
  else:
    a = empty(value, dtype=dtype)
          
  return a      


############################################################################
# Class: Filter
#
# Implements Array Based filters with dictionary/array filter storage
############################################################################
class Filter(Array):
  def __init__(self, arg, dtype=np.double):
    self.center     = None
    self.filterDict = {}
    
    if isinstance(arg,dict):
      self.filterDict = arg
      self.dtype      = dtype
      self.setArray()
    elif isinstance(arg,Array):
      super(Filter,self).__init__(arg.shape, dtype=dtype)
      self.array[:] = arg.array[:]
      self.setDict()
    elif isinstance(arg, numbers.Integral):
      super(Filter,self).__init__((arg,arg), dtype=dtype)
      self.array[:] = 0
      self.filterDict = {}
    else:
      raise TypeError("Filter is initialized by one arg, a dict of coord " +
                      "keys + values, an asa.Array of values, or a " +
                      "dimension for an empty (d,d) filter.")
      
    self.center = centerOfRect(self.shape)
    return

  def setDict(self):
    ctr = centerOfRect(self.shape)
    asaMat = np.vstack(np.nonzero(self.array)).transpose()
    fCrd = [ coord(crd) - ctr for crd in asaMat ]

    self.filterDict = {}
    for idx in xrange(len(fCrd)):
      self.filterDict[coord(asaMat[idx,:])] = self[coord(asaMat[idx,:])]
    return
      
  def setArray(self, debug=False):
    asaDict = self.filterDict
    dtype   = self.dtype
    
    f2dCrd = np.array([ c.as2d for c in asaDict ],dtype=np.int)
    minRC = f2dCrd.min()
    maxRC = f2dCrd.max()
    d = maxRC - minRC + 1

    super(Filter,self).__init__((d,d), dtype=dtype)
    self.array[:] = 0
    ctr = centerOfRect((d,d))

    for fCrd in asaDict:
      self[fCrd + ctr] = asaDict[fCrd]
    return

  @property
  def dict(self):
    return self.filterDict

  
############################################################################
# Filter Support Functions
############################################################################

# Given a radius, rad in hex units (float), return a filer which
# contains only hex centers falling within that radius.
def makeCircFilter(rad, normalize=True):
  # The filter will be d by d where d is odd
  d  = int(1 + 2 * math.floor(rad))

  flt = zeros((d,d),dtype=np.double)
  ctr = centerOfRect((d,d))

  fCrd = np.array([[r%2, r/2, c] for r in xrange(d) for c in xrange(d)],
                  dtype=np.int)
  dAry = np.array(dist([ctr for i in xrange(d*d)], fCrd))
  
  insideIdxs = np.nonzero(dAry <= rad)
  nIdxs = len(insideIdxs[0])

  for idx in insideIdxs[0]:
    flt[fCrd[idx]] = 1

  if normalize:
    flt.array = flt.array/flt.array.sum()

  return Filter(flt)


# Given a delta-resolution, dRes, return an aperaturature 4 downsample
# filter which averages together those fine-res pixels which fall
# strictly within or on th e edge of the coarse-res parent pixel.  This
# filter should be excellent for both finding the pyramid children and
# doing the actual downsample computation from n -> (n - dRes).
def makeA4DSHexFilter(dRes, normalize=True):
  rad =  2**(dRes-1)
  
  apr4Max    = makeA4DSMaxFilter(dRes)
  apr4Dict   = apr4Max.filterDict
  asaKeyMat  = np.array([tuple(k) for k in apr4Dict ], dtype=np.int)
  cartKeyMat = np.array(toCart(asaKeyMat), dtype=np.double)

  cartC = np.abs(cartKeyMat[:,0])
  cartR = np.abs(cartKeyMat[:,1])

  eps = 0.01
  # Find the elements of the filter that lie within and on the dRes hex.
  # inside or on the vertical hex edge
  condInsideV = (rad + eps) > cartC
  # inside or on the sloped hex edge
  condInsideSlp = (2*rad/np.sqrt(3) + -cartC/np.sqrt(3) + eps) > cartR
  # on the vertical hex edge
  condOnV = np.abs(rad - cartC) < eps
  # on the sloped hex edge
  condOnSlp = np.abs(2*rad/np.sqrt(3) + -cartC/np.sqrt(3) - cartR) < eps

  # filter items outside of hex
  badIdxs = np.nonzero(np.logical_not(
                         np.logical_and(condInsideV, condInsideSlp)))[0]
  for idx in badIdxs:
    del apr4Dict[coord(asaKeyMat[idx,:])]

  # get items inside or on the edge
  goodIdxs = np.nonzero(np.logical_and(condInsideV, condInsideSlp))[0]
  # get items which are only on the edges
  edgeIdxs = np.nonzero(np.logical_and(
                          np.logical_and(condInsideV, condInsideSlp),
                          np.logical_or(condOnV, condOnSlp)))[0]
  # set filter values
  fltSum = 0.0
  # assign all hexels a value of 1.0
  for idx in goodIdxs:
    apr4Dict[coord(asaKeyMat[idx,:])] = 1.0
    fltSum += 1.0
  # for edge hexels assign a value of 0.5 and subtract from the filter
  # sum
  for idx in edgeIdxs:
    apr4Dict[coord(asaKeyMat[idx,:])] = 0.5
    fltSum -= 0.5

  # if we are normalizing, divide each item by the filter sum
  if normalize:
    for k in apr4Dict:
      apr4Dict[k] /= float(fltSum)

  # return result as an asa filter
  return Filter(apr4Dict)

# This creates a set of filters which if applied in sequence on a
# resolution n grid will result in a resolution n-dRes grid after
# selection of resulting points. The idea being that it may touch fewer
# NaNs.  However, this is strictly more computationally intensive than
# makeRecurA4DSMaxFilter but should return identical results..
def makeA4DSFilterSet(asa0, dRes):
  fltSet = collections.OrderedDict()

  for n in xrange(1,dRes+1):
    cDict = collections.defaultdict(float)
    cDict[coord(asa0) + (2**(n-1) * coord(0,0,1))] = 0.125
    cDict[coord(asa0) + (2**(n-1) * coord(1,0,0))] = 0.125
    cDict[coord(asa0) + (2**(n-1) * coord(1,0,-1))] = 0.125
    cDict[coord(asa0) + (2**(n-1) * coord(0,0,-1))] = 0.125
    cDict[coord(asa0) + (2**(n-1) * coord(1,-1,-1))] = 0.125
    cDict[coord(asa0) + (2**(n-1) * coord(1,-1,0))] = 0.125
    cDict[coord(asa0)] = 0.25
    fltSet[n] = Filter(cDict)

  return fltSet

# Use the recursive application of the 6-connected neighbors at 1-res
# spacing to return the filter which is equivalent to dRes applictions
# of successive downsampling.
def makeA4DSMaxFilter(dRes, normalize=True, debug=False):
  if not normalize:
    raise DepricatonWarning('all filters created by ' +
                            'makeA4DSMaxFilter() are normalized.')
  
  return Filter(makeRecurA4DSDict(coord(0,0,0),dRes))

# Recursive function to support makeA4DSMaxFilter()
def makeRecurA4DSDict(asa0, dRes):
  cDict = collections.defaultdict(float)
  cDict[coord(asa0) + (2**(dRes-1) * coord(0,0,1))] = 0.125
  cDict[coord(asa0) + (2**(dRes-1) * coord(1,0,0))] = 0.125
  cDict[coord(asa0) + (2**(dRes-1) * coord(1,0,-1))] = 0.125
  cDict[coord(asa0) + (2**(dRes-1) * coord(0,0,-1))] = 0.125
  cDict[coord(asa0) + (2**(dRes-1) * coord(1,-1,-1))] = 0.125
  cDict[coord(asa0) + (2**(dRes-1) * coord(1,-1,0))] = 0.125
  cDict[coord(asa0)] = 0.25

  if dRes == 1:
    return cDict

  retDict = collections.defaultdict(float)
  for c in cDict.keys():
    rcDict =  makeRecurA4DSDict(c,dRes-1)
    for rc in rcDict.keys():
      rcDict[rc] *= cDict[c]
      retDict[rc] += rcDict[rc]

  return retDict

# Given image, I, and filter, M, convolve the filter with the image and
# return an asaArray of identical size to I.  This result should be only
# the valid unpadded result centered in I.
def convolveFilter(I, M, dbg=False):
  (iH, iW) = (I.h, I.w)
  iA0 = I.a0
  iSh = I.subArrayHeight
  iA1 = I.fullArrayHeight - iH
  (mH, mW) = (M.h, M.w)
  mA0 = M.a0
  mSh = M.subArrayHeight
  mA1 = M.fullArrayHeight - mH

  I0 = I.array[0,0:iSh,:]
  I1 = I.array[1,0:iSh,:]

  if dbg:
    print("I0 shape: %s" % str(I0.shape))
    print("I1 shape: %s" % str(I1.shape))
  (hI0,wI0) = I0.shape
  (hI1,wI1) = I1.shape
  
  # Note: M0 and M1 are swapped since the center row of the filter is
  # redefined as the 0 array
  # Due to the fact that the filter is centered on the M0 row, the M1
  # row must be padded on the right so that center of the filter is
  # properly located.
  M0  = M.array[(mH/2)%2,0:mSh-mA1*((mH/2)%2),:]
  # M1  = M.array[(1+mH/2)%2,0:mSh-mA1*((1+mH/2)%2),:]
  M1         = np.zeros((mSh-mA1*((1+mH/2)%2),(mW%2)+mW), dtype=np.double)
  M1[:,(mW/2+1)%2:mW+(mW/2+1)%2] = M.array[(1+mH/2)%2,0:mSh-mA1*((1+mH/2)%2),:]

  if dbg:
    print("M0 shape: %s" % str(M0.shape))
    print(M0)
    print("M1 shape: %s" % str(M1.shape))
    print(M1)
  (hM0, wM0) = M0.shape
  (hM1, wM1) = M1.shape

  boundary = 'fill'
  mode00 = 'full'
  mode01 = 'same'
  mode10 = 'full'
  mode11 = 'full'
  # Catch and ignore the 'cast to real' warning.
  with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    J00 = scipy.signal.convolve2d(I0, M0, mode=mode00, boundary=boundary) 
    J01 = scipy.signal.convolve2d(I1, M1, mode=mode01, boundary=boundary)
    J10 = scipy.signal.convolve2d(I1, M0, mode=mode10, boundary=boundary)
    J11 = scipy.signal.convolve2d(I0, M1, mode=mode11, boundary=boundary)

  if dbg:
    print("Prior to extraction")
    print("J00 shape: %s" % str(J00.shape))
    print("J01 shape: %s" % str(J01.shape))
    print("J10 shape: %s" % str(J10.shape))
    print("J11 shape: %s" % str(J11.shape))
  (hJ00, wJ00) = J00.shape
  (hJ01, wJ01) = J01.shape
  (hJ10, wJ10) = J10.shape
  (hJ11, wJ11) = J11.shape

  # This mod works for (3,3) filter                 
  #  J11 = J11[1:,-(-mW/2):-(-mW/2)+wJ10]
  if mode00 == 'full':
    J00 = J00[hM0/2:hI0+hM0/2,wM0/2:wI0+wM0/2]
  if mode01 == 'full':
    J01 = J01[hM1/2:hI0+hM1/2,wM1/2:wI0+wM1/2]
  if mode10 == 'full':
    J10 = J10[hM0/2:hI0+hM0/2,wM0/2:wI0+wM0/2]
  if mode11 == 'full':
    J11 = J11[hM1/2:hI0+hM1/2,wM1/2:wI0+wM1/2]

  if dbg:
    print("Post extraction")
    print("J00 shape: %s" % str(J00.shape))
    print("J01 shape: %s" % str(J01.shape))
    print("J10 shape: %s" % str(J10.shape))
    print("J11 shape: %s" % str(J11.shape))

  J0 = J00 + J01
  J1 = J10 + J11

  J = empty((iH, iW), dtype=I.dtype, a0=iA0)
  J.array[0,0:iSh,:] = J0.real
  J.array[1,0:iSh,:] = J1.real

  return J

# Given image, I, filter, M, and asaLocation, apply the filter once.
# This would be prohibitively expensive for convolution but is
# implemented and tested to provide a check for the convolveFilter()
# function.
def applyFilterOnce(I, M, asaLoc, dbg=False):
  (mH, mW)  = (M.h, M.w)
  mH2       = mH/2
  mOdd      = (mH2)%2
  mSh       = M.subArrayHeight                 
  mA1       = M.fullArrayHeight - mH

  (al,rl,cl) = asaLoc.coord
  iOdd = al

  ul = asaLoc - centerOfRect((mH,mW))
  lr = asaLoc + centerOfRect((mH,mW))
  (a0,r0,c0)  = ul.coord
  (r02d,c02d) = ul.as2d
  (aF,rF,cF)  = lr.coord
  (rF2d,cF2d) = lr.as2d
  # Test to see if M crosses the edge of I
  if (r02d < 0 or c02d < 0 or
      rF2d >= I.h or cF2d >= I.w):
    return np.NAN
  
  I_sub = I.get2dSlice((slice(r02d,r02d+mW,None),
                        slice(c02d,c02d+mH,None)),
                        rebase=True)
  
  if al == 0:
    I0 = I_sub.array[mOdd,:mSh-mA1*mOdd,:]
    I1 = I_sub.array[(mOdd+1)%2,:mSh-mA1*((mOdd+1)%2),:]
  else:
    I0 = I_sub.array[(mOdd)%2,:mSh-mA1*((mOdd)%2),:]
    I1 = I_sub.array[(mOdd+al)%2,:mSh-mA1*((mOdd+al)%2),:]
    
  M0   = M.array[mOdd,0:mSh-mA1*(mOdd),:]
  M1   = M.array[(1+mOdd)%2,0:mSh-mA1*((1+mOdd)%2),:]

  if dbg:
    print(I0); print(I1)
    print(M0); print(M1)
  
  R0 = I0.ravel() * M0.ravel()
  R0_valid = R0[np.nonzero(M0.ravel())]
  if len(M1) != 0:
    R1 = I1.ravel() * M1.ravel()
    R1_valid = R1[np.nonzero(M1.ravel())]
  else:
    R1_valid = np.zeros((1,))

  return R0_valid.sum() + R1_valid.sum()


              
############################################################################
# Class: AsaCoord
#
# Implements ASA Coordinates and associated mathematical functions for
# Indexing Hexagonal Arrays
############################################################################
class AsaCoord(object):
  def __init__(self,t):
    try:
      (a,r,c) = tuple(t)
    except:
      raise Exception("must recieve a tuple coercable object of len 3.")
    self.coord = np.array([a,r,c], dtype=np.int64)

  def __str__(self):
    return "asa(%d,%d,%d)" % tuple(self.coord)
    
  def __repr__(self):
    t = type(self)
    mod = t.__module__
    name = t.__name__
    strList = [ mod ] + list(tuple(self.coord))
    return "%s.coord(%d,%d,%d)" % tuple(strList)

  def __hash__(self):
    return hash(tuple(self.coord))
    
  def __len__(self):
    return len(self.coord)
  
  def __getitem__(self,i):
    try:
      i = int(i)
    except:
      raise TypeError("item must be coercable to 'int'")
    
    if (i < 0) or (i > 2):
      raise KeyError("item must be in [0, 2] inclusive")
    return self.coord[i]
  
  def __iter__(self):
    return iter(self.coord)
    
  def __neg__(self):
    (a0, r0, c0) = self.coord       
    return AsaCoord((a0,
                     -r0 - a0,
                     -c0 - a0))

  def __eq__(self, other):
    return tuple(self.coord) == tuple(other.coord)

  def __ne__(self, other):
    return not self == other
  
  def __add__(self, other):
    if type(other) != type(self):
      raise TypeError("unsupported operand type(s) for +: '%s' and '%s'" %
                      (type(self).__name__, type(other).__name__))
                       
    (a0, r0, c0) = self.coord
    (a1, r1, c1) = other.coord
    return AsaCoord(((a0 ^ a1),
                    (r0 + r1 + (a0 & a1)),
                    (c0 + c1 + (a0 & a1))))

  def __radd__(self, other):
    raise TypeError("unsupported operand type(s) for +: '%s' and '%s'" %
                      (type(other).__name__, type(self).__name__))
  
  def __sub__(self, other):
    if type(other) != type(self):
      raise TypeError("unsupported operand type(s) for +: '%s' and '%s'" %
                      (type(self).__name__, type(other).__name__))
    return self + (-other)

  def __rsub__(self, other):
    raise TypeError("unsupported operand type(s) for +: '%s' and '%s'" %
                      (type(other).__name__, type(self).__name__))

  def __mul__(self, other):
    isReal = isinstance(other, numbers.Real)
    if not isReal:
      raise TypeError("unsupported operand type(s) for *: '%s' and '%s'" %
                      (type(self).__name__, type(other).__name__))
    if other >= 0:
      (a,r,c) = self.coord
      k = other
      return AsaCoord(((a*k) % 2,
                       k*r + a*(k/2),
                       k*c + a*(k/2)))
    else:
      return -self * -other

  def __rmul__(self,other):
    isReal = isinstance(other, numbers.Real)
    if not isReal:
      raise TypeError("unsupported operand type(s) for *: '%s' and '%s'" %
                      (type(other).__name__, type(self).__name__))
    return self * other
  
  @property
  def a(self):
    return self.coord[0]

  @property
  def r(self):
    return self.coord[1]

  @property
  def c(self):
    return self.coord[2]

  @property
  def as2d(self):
    (a, ar, ac) = self.coord
    r = (2*ar) + a
    c = ac
    return (r,c)

  @property
  def norm(self):
    "Return the vector length i.e. the hex-Manhattan distance."
    U = self.c - self.r
    V = self.a + 2*self.r
    if U*V > 0:
      return abs(U) + abs(V)
    else:
      return max(abs(U), abs(V))

    
############################################################################
############################################################################
# Support Functions for ASA Coordinates
############################################################################
############################################################################

############################################################################
# coord(): given 3 args, or an object coercable into a 3-tuple, return a
# corresponding AsaCoord.
############################################################################
def coord(*args):
  nargs = len(args)
  if nargs == 1:
    tup = args[0]
  elif nargs == 2:
    tup = (args[0]%2, args[0]/2, args[1])
  elif nargs == 3:
    tup = args
  else:
    raise TypeError("expects a tuple, 2 args (asa.as2d coord), or 3 args")
  
  return AsaCoord(tup)

############################################################################
# toCart(): Given an asa coordinate, np.array of coordinates, or a list
# of coordinates, return a cartesian coordinate or list of cartesian
# coordinates.
############################################################################
def toCart(asa):
  singleton = (not isinstance(asa, list) and
               not isinstance(asa, np.ndarray))
  if singleton:
    asaList = [ asa ]
  else:
    asaList = asa
  
  asaArr = np.array(asaList, dtype=np.double)

  convArr = np.array([[             0.5,            0, 1],
                      [sqrt(3).real/2.0, sqrt(3).real, 0]], dtype=np.double)

  cart = np.dot(convArr,asaArr.transpose())

  if singleton:
    return tuple(cart.transpose()[0,:])
  else:
    return [ tuple(c) for c in cart.transpose() ]

############################################################################
# dist(): given 2 asa coords or 2 lists of asa coords, determine the
# euclidian distance between those coords and return the single distance
# or a list of distances.
############################################################################
def dist(asa0, asa1):
  cart0 = toCart(asa0) 
  cart1 = toCart(asa1)

  singleton = not isinstance(cart0, list)
  if singleton:
    cart0 = [ cart0 ]
    cart1 = [ cart1 ]

  cartArr0 = np.array(cart0, dtype=np.double)
  cartArr1 = np.array(cart1, dtype=np.double)

  distSqArr = (cartArr1 - cartArr0) * (cartArr1 - cartArr0)
  distArr = np.sqrt(distSqArr[:,0] + distSqArr[:,1])

  if singleton:
    return distArr[0]
  else:
    return [ d for d in distArr[:] ]

############################################################################
# norm(): given 2 asa coords or 2 lists of asa coords, determine the
# hex-Manhattan distance between those coords and return the single
# distance or a list of distances.
############################################################################
def norm(asa0, asa1):
  singleton = not isinstance(asa0, list)
  if singleton:
    asa0 = [ asa0 ]
    asa1 = [ asa1 ]

  asaArr0 = np.array(asa0, dtype=np.int)
  asaArr1 = np.array(asa1, dtype=np.int)
  (a0, r0, c0) = (asaArr0[:,0], asaArr0[:,1], asaArr0[:,2])
  (a1, r1, c1) = (asaArr1[:,0], asaArr1[:,1], asaArr1[:,2])
  
  U = (c1 - c0) - (r1 - r0)
  V = (a1 - a0) + 2*(r1 - r0)

  UV = U * V
  
  sameSignIdxs = np.nonzero(UV > 0)
  elseIdxs     = np.nonzero(np.logical_not(UV > 0))

  R = np.zeros(U.size, dtype=np.int)
  R[sameSignIdxs] = np.abs(U[sameSignIdxs]) + np.abs(V[sameSignIdxs])
  R[elseIdxs] = np.vstack([np.abs(U[elseIdxs]),
                             np.abs(V[elseIdxs])]).max(axis=0)
  if singleton:
    return R[0]
  else:
    return [ n for n in R[:] ]
    
############################################################################
# centerOfRect(): given a 2-tuple, (h, w) describing a "rectangle" of
# hexagonal plane space, return the center AsaCoord of that "rectangle."
# This is needed for placing the filter center within an Array filter.
# This assumes that the rect array is even-row-start.
############################################################################
def centerOfRect(size):
  (h, w) = size
  w2 = w/2
  h2 = h/2

  v0 = AsaCoord((0,0,w2))
  v1 = AsaCoord((h2 % 2, h2 / 2, 0)) 
  return v0 + v1
  
