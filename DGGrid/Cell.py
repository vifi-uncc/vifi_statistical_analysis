# Copyright [2007]. California Institute of Technology.  ALL RIGHTS RESERVED.
# U.S. Government sponsorship acknowledged. Any commercial use must be
# negotiated with the Office of Technology Transfer at the California
# Institute of Technology.

# This software is subject to U. S. export control laws and regulations
# (22 C.F.R. 120-130 and 15 C.F.R. 730-774). To the extent that the
# software is subject to U.S. export control laws and regulations, the
# recipient has the responsibility to obtain export licenses or other
# export authority as may be required before exporting such information
# to foreign countries or providing access to foreign nationals.

# Debugging, if needed...
# import pdb
#
# Log everything, and send it to stderr.
# import logging
# logging.basicConfig(level=logging.DEBUG)

# Numerical Python
import numpy as np

# Get tools for line simplification and great circle geometry
from .vis.PolyTools import *

# Get globals
from .config import *


##############################################################################
# Class: Cell
#
# This class implements a cell or footprint within a DGG.
##############################################################################
class Cell(object):
  def __init__(self, grid=None, center=None, line=None, idx=None, q2di=None,
               type='lazy', footprint=None):
    self.grid      = grid
    self.footprint = footprint
    self.cellType  = type # { lazy, dgg, footprint }
    
    self._center   = center              # tuple - (lat, lon)
    self._polyLine = line                # list of tuples - (lat, lon)

    self.idx     = idx
    self.q2di    = q2di
    if q2di is not None:
      (q,i,j) = q2di
      self.q2diInt = ("%02d%s" % (q,
                       str(2 * int("{0:b}".format(i)) +
                           int("{0:b}".format(j))).zfill(grid.resolution)))
    else:
      self.q2diInt = None

  @property
  def q2diTup(self):
    if (self.grid is not None) and (self.grid.idxCellQ2diMat is not None):
      return tuple(self.grid.idxCellQ2diMat[self.idx,:])
    else:
      return None

  @property
  def asaTup(self):
    if (self.grid is not None) and (self.grid.idxCellAsaMat is not None):
      return tuple(self.grid.idxCellAsaMat[self.idx,:])
    else:
      return None

  @property
  def center(self):
    if (self.grid is not None) and (self.grid.idxCellCenterMat is not None):
      # All arrays of geo-coords are (lon, lat), but center is a (lat, lon)
      return tuple(self.grid.idxCellCenterMat[self.idx,::-1])
    else:
      if self._center is not None:
        return self._center
      elif self.cellType == 'lazy':
        cArry = dgglibc.q2di_to_centers(dggLibCState, self.grid.resolution,
                                        np.array([self.q2diTup]))
        return tuple(cArry[0,::-1])
      else:
        print('ERROR: cell has no center info -- malformed cell.')

  @property
  def polyLine(self):
    if (self.grid is not None) and (self.grid.idxCellPolyMat is not None):
      # All arrays of geo-coords are (lon, lat), but polyLine is a list
      # of (lat, lon)
      poly = [tuple(p)
              for p in self.grid.idxCellPolyMat[self.idx,::-1,::-1] ]
      # Some polygons are pentagons and not hexagons, so we need to trim them
      if self.q2diTup[1:] == (0,0):
        trim = self.grid.densify + 1
      else:
        trim = 0        
      poly = poly[trim:]

      return poly
    else:
      if self._polyLine is not None:
        return self._polyLine
      elif self.cellType == 'lazy':
        raise TypeError("no polyLine for 'lazy' cells, use cell.geoData for " +
                        "lazy generation.")
      else:
        print('ERROR: cell has no poly line info -- malformed cell.')
        return False

  @property
  def geoData(self):
    if (self.cellType == 'dgg' or
        self.cellType == 'footprint'):
      return (self.center, self.polyLine)
    
    # q2diMat = np.array([self.q2di], dtype=np.int64)
    res = self.grid.resolution
    den = self.grid.densify
    (c,pl) = dgglibc.q2di_to_geo_coords(dggLibCState, res, den,
                                        np.array([self.q2di],dtype=np.int64))
    pList = [tuple(p) for p in pl[0,::-1,::-1]]
    # Some polygons are pentagons and not hexagons, so we need to trim them
    if self.q2diTup[1:] == (0,0):
      trim = self.grid.densify + 1
    else:
      trim = 0        
    pList = pList[trim:]
    cTup  = tuple(c[0,::-1])

    return (cTup, pList)

  @property
  def getLonLatBox(self):
    if self.cellType == 'dgg':
      (minLat, minLon) = np.array(self.polyLine).reshape((-1,2)).min(axis=0)
      (maxLat, maxLon) = np.array(self.polyLine).reshape((-1,2)).max(axis=0)
    else:
      # q2diMat = np.array([self.q2di], dtype=np.int64)
      res = self.grid.resolution
      den = self.grid.densify
      (c,pl) = dgglibc.q2di_to_geo_coords(dggLibCState, res, den,
                                          np.array([self.q2di],dtype=np.int64))
      (minLon, minLat) = np.array(pl).reshape((-1,2)).min(axis=0)
      (maxLon, maxLat) = np.array(pl).reshape((-1,2)).max(axis=0)

    # Fix anti-meridian spanning
    diffLon = maxLon - minLon
    if (diffLon) > 180:
      # tmp    = minLon
      minLon = maxLon
      maxLon = maxLon + (360 - diffLon)

    return (minLon, minLat, maxLon, maxLat)
  
  def dumpKml(self, data=None, outline=False, kmlRegInfo=None,
              bluePents = False):
    # Assumes that ctr is (lat, lon) 
    # Assumes that polyline is a list of (lat, lon) tuples
    (ctr, poly) = self.geoData

    # KML wants tuples in (lon, lat) order
    center    =  "%f,%f" % (ctr[1], ctr[0])

    # KML requires the first and last points to repeat
    poly.append(poly[0])

    # Detect dateline span then fix spanning polygons
    if doesSpanAntimeridian(poly):
      pArry = np.array(poly, dtype=np.double)
      idxs = np.nonzero(pArry[:,1] < 0.0)
      pArry[idxs,1] = 360 + pArry[idxs,1]
      poly = [ tuple(pArry[i,:]) for i in xrange(pArry.shape[0]) ]
      
    # KML wants tuples in (lon, lat) order
    coordList =  ",".join([ ("%f,%f,%d" % (p[1], p[0], KML_HEIGHT))
                             for p in poly])

    if kmlRegInfo is None:
      kmlRegionStr = ""
    else:
      (minLodPix, maxLodPix, minFade, maxFade) = kmlRegInfo
      (minLon, minLat, maxLon, maxLat) = self.getLonLatBox
      d = {}
      d['minLon'] = minLon
      d['minLat'] = minLat
      d['maxLon'] = maxLon
      d['maxLat'] = maxLat
      d['minLodPix'] = minLodPix
      d['maxLodPix'] = maxLodPix
      d['minFade'] = minFade
      d['maxFade'] = maxFade
      kmlRegionStr = KML_REGION % d

 
    d = {}
    d['coordlist'] = coordList
    d['center'] = center

    # Default cell color for no data grids
    if (self.q2di is not None) and bluePents:
      (q,i,j) = self.q2di
      if (i == 0) and (j == 0):
        d['cellColor'] = '7fff0000' # somewhat opaque - '7f0000ff'
    else:
      d['cellColor'] = '7f0000ff' # somewhat opaque - '7f0000ff'

    d['kmlRegion'] = kmlRegionStr
    if self.cellType == 'dgg':
      d['name'] = ("Cell(dgg): %d, q2di: (%d,%d,%d), asa: (%d,%d,%d)" %
                   (self.idx,
                    self.q2diTup[0], self.q2diTup[1], self.q2diTup[2],
                    self.asaTup[0], self.asaTup[1], self.asaTup[2]))

      if data is not None:
        d['cellColor'] = data.getKmlColorByCellId(self.idx)
    elif self.cellType == 'lazy':
      d['name'] = ("Cell(lazy): %d, q2di: (%d,%d,%d)" %
                   (self.idx,
                    self.q2diTup[0], self.q2diTup[1], self.q2diTup[2]))

      if data is not None:
        d['cellColor'] = data.getKmlColorByCellId(self.idx)
    elif self.cellType == 'footprint':
      d['name'] = ("Footprint: (%f, %f) by %f km" %
                   (self.center[0], self.center[1], self.footprint.radiusKm))
      
      d['cellColor'] = self.footprint.getKmlColorSamp(self.idx)
    else:
      d['name'] = "Cell Type: Unknown"

    if outline:
      buf = (KML_POLY_OUTL % d)
    else:
      buf = (KML_POLY % d)

    return buf


## class PyrLvlCell(Cell):
##   def __init__(self):
##     self.dataType  = 'pyr-lvl'
##     self.pyramid   = pyr
##     self.grid      = grid
##     self.childGrid = None

##     self.childCellList = None

##   def dumpKml(self):
##     buf = None
##     # write out this cell's poly and color

##     # write out appropos pyramid level info with links to sub cell KML
##     # files

##     # return KML
##     return buf
