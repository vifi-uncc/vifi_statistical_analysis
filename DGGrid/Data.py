# Copyright [2007]. California Institute of Technology.  ALL RIGHTS RESERVED.
# U.S. Government sponsorship acknowledged. Any commercial use must be
# negotiated with the Office of Technology Transfer at the California
# Institute of Technology.

# This software is subject to U. S. export control laws and regulations
# (22 C.F.R. 120-130 and 15 C.F.R. 730-774). To the extent that the
# software is subject to U.S. export control laws and regulations, the
# recipient has the responsibility to obtain export licenses or other
# export authority as may be required before exporting such information
# to foreign countries or providing access to foreign nationals.

# Debugging, if needed...
# import pdb
#
# Log everything, and send it to stderr.
# import logging
# logging.basicConfig(level=logging.DEBUG)

import os.path as osp
import math
import numbers

# Import needed numeric python support
import numpy as np
import scipy
import scipy.stats as stat
import scipy.misc as misc
from matplotlib import cm

# ASA hex arrays and asa coordinates
from .AsaHexArray import *

# File reading and grid tools
from .ReaderTools import *

# Get tools for line simplification and great circle geometry
from .vis.GeoTools import *

##############################################################################
# Class: Data
#
# This class implements the parent class and subclasses to support
# loading data associated with a grid and mapping it to an ASA array.
##############################################################################
class Data(object):
  def __init__(self):
    self.dataType    = 'simple'
    self.grid        = None
    self.RawData     = None
    self.data        = None
    self.cellIdxDict = None
    self.sampleIdx0  = None
    self.sampleIdx   = None
    self.sampleMin   = None
    self.sampleMax   = None
    self.sampleMean  = None
    
    self.asaSampList          = []
    self.asaArrayDictBySample = {}

    # Is our data ZERO_CENTERED
    self.ZERO_CENTERED_SCALE=False

  @property
  def asaArray(self):
    try:
      return self.asaArrayDictBySample[self.sampleIdx - self.sampleIdx0]
    except KeyError:
      raise NameError("no asaArray has been defined. try makeAsaArrays() first.")

  def setDataMatrix(self, grid, dataMatrix):
    self.grid       = grid
    self.RawData    = dataMatrix
    self.data       = self.RawData
    self.sampleIdx0 = 2
    self.sampleIdx  = 2

    # Check if lon/lat passes sniff test
    if not(isValidLons(self.data[:,0]) and isValidLats(self.data[:,1])):
      raise NameError("Error, file has invalid lon/lat. ")

    self.cellIdxDict = grid.lonLatMat2cellIdxDict(self.data[:,0:2])
    self.characterizeData(debug=True)

    if self.dataType == 'asa':
      self.asaArrayDict = {}
      self.asaMakeArrays()
    
  def setSampleIdx(self, samp):
    self.sampleIdx = self.sampleIdx0 + samp
    self.characterizeData()

  def characterizeData(self, force=None, pctile=None, debug=False):
    sampleIdx = self.sampleIdx

    if force is None:
      if pctile is None:
        # Assigned Min/Max to get appropriate color bar
        self.sampleMin  = self.data[:, sampleIdx].min()
        self.sampleMax  = self.data[:, sampleIdx].max()
        self.sampleMean = self.data[:, sampleIdx].mean()
      else:
        lo = pctile
        hi  = 100 - pctile
        self.sampleMin  = stat.scoreatpercentile(self.data[:, sampleIdx], lo)
        self.sampleMax  = stat.scoreatpercentile(self.data[:, sampleIdx], hi)
        self.sampleMean = self.data[:, sampleIdx].mean()
      if debug:
        print ("Color (sample %d) Min: %f, Max: %f, Mean: %f" %
               (sampleIdx - self.sampleIdx0, self.sampleMin, self.sampleMax,
                self.sampleMean))
    else:
      (self.sampleMin, self.sampleMax, self.sampleMean) = force
      if debug:
        print ("Forced (sample %d) Min: %f, Max: %f, Mean: %f" %
               (sampleIdx - self.sampleIdx0, self.sampleMin, self.sampleMax,
                self.sampleMean))

  def makeAsaArrays(self, samp = None, inclPad = True):
    if self.dataType == 'asa':
      print ("makeAsaArrays(): nop, dataType already 'asa'")
      return
    
    if ((self.cellIdxDict is None) or
        (self.data is None) or
        (self.grid is None)):
      raise NameError("makeAsaArray():ERROR: Need cellIdxDict, data, " +
                      "and grid.")
    
    g = self.grid

    if ((g.gridType is not 'asa') or
        (g.cellAsaDict is None)):
      raise NameError("makeAsaArray():ERROR: Parent grid must have complete" +
                      "asa mapping")
    h = g.asaHeight
    w = g.asaWidth

    # get the list of all valid (a,r,c) tuples including all padding
    # less any pents or other NaN areas
    if inclPad:
      arcList     = g.cellAsaDict.keys()
    else:
      arcList     = [ tuple(arc) for arc in g.idxCellAsaMat ]
    # get a generator for cell idx parallel to the arcList
    cellIdxGen    = ( g.cellAsaDict[arc].idx for arc in arcList )
    # get a list for cid linking the data idxs to cell idx
    # parallel to the arcList
    dataIdxLstList = [ self.cellIdxDict[cid] for cid in cellIdxGen ]

    # sampList is a list of sample numbers referenced from the
    # sampleIdx0.  Here we check 'samp' which overrides the default
    # sample or the structure sample list.
    if isinstance(samp, numbers.Integral):
      sampList = [ samp ]
    elif isinstance(samp, list):
      sampList = samp 
    elif len(self.asaSampList) > 0:
      sampList = self.asaSampList
    else:
      sampList = [ self.sampleIdx - self.sampleIdx0 ]

    self.asaSampList = sampList
    self.asaArrayDictBySample = {}
      
    for sample in sampList:
      # print "makeAsaArrays(): processing sample %d" % sample
      arcIdx = 0
      asaArrayRef = empty((h,w), dtype=np.double)
      for dataIdxLst in dataIdxLstList:
        if len(dataIdxLst) == 0:        
          asaArrayRef[arcList[arcIdx]] = np.NAN
        elif len(dataIdxLst) == 1:
          asaArrayRef[arcList[arcIdx]] = self.data[dataIdxLst[0],
                                                   sample + self.sampleIdx0 ]
        else:
          asaArrayRef[arcList[arcIdx]] = self.data[dataIdxLst,
                                                   (sample +
                                                    self.sampleIdx0)].mean()
        arcIdx += 1 

      self.asaArrayDictBySample[sample] = asaArrayRef
     
    self.dataType = 'asa'

  def refreshAsaArray(self, asaArray, debug=True):
    if ((self.cellIdxDict is None) or
        (self.grid is None)):
      raise NameError("refreshAsaArray():ERROR: Need cellIdxDict " +
                      "and grid.")
    
    g = self.grid

    if ((g.gridType is not 'asa') or
        (g.cellAsaDict is None)):
      raise NameError("refreshAsaArray():ERROR: Parrent grid must have " +
                      "complete asa mapping")

    # get the list of all valid (a,r,c) tuples including all padding
    # less any pents or other NaN areas
    arcList       = g.cellAsaDict.keys()
    # get a generator for cell idx parallel to the arcList
    cellIdxGen    = ( g.cellAsaDict[arc].idx for arc in arcList )
    # get a list for cid linking the data idxs to cell idx
    # parallel to the arcList
    dataAsaLst = [ g.cellList[cid].asaTup for cid in cellIdxGen ]

    nanAsaMat = np.vstack(np.nonzero(np.isnan(asaArray.array))).transpose()
    nanAsaFix = set(arcList).intersection([ tuple(arc) for arc in nanAsaMat ])
    if debug:
      nNaNs0  = nanAsaMat.shape[0]
      fixable = len(nanAsaFix)
      print("Found %d NaNs, %d fixable" % (nNaNs0, fixable))
    for arc in nanAsaFix:
      idx = arcList.index(arc)
      asaArray[arc] = asaArray[dataAsaLst[idx]]

    if debug:
      nanAsaMat = np.vstack(np.nonzero(np.isnan(asaArray.array))).transpose()
      nNaNs = nanAsaMat.shape[0]
      print("  %d fixed, %d NaNs remain." % (nNaNs0 - nNaNs, nNaNs))
    return
      
  def extractDataNearestNeighbor(self, lonLatMat=None,
                                 locCellIdxDict=None,
                                 samp=0):
    g = self.grid

    if locCellIdxDict is None:
      # Get dictionary linking each row to DGGrid Cell
      cellIdxDict = g.lonLatMat2cellIdxDict(lonLatMat)
      nPoints = lonLatMat.shape[0]
    else:
      cellIdxDict = locCellIdxDict[0]
      nPoints = locCellIdxDict[1]
      

    # Convert from cell dict to list of values
    nnDataValueArry = np.zeros((nPoints,), dtype=np.double)
    for k in cellIdxDict.keys():
      idxs = cellIdxDict[k]
      for i in idxs:
        nnDataValueArry[i] = self.getByCellId(k, samp = samp)

    return nnDataValueArry

  
  def extractDataRadius(self, radiusKm, lonLatMat=None, locCellIdxDict=None,
                        samp=0):
    g = self.grid

    if locCellIdxDict is None:
      # Get dictionary linking each row to DGGrid Cell
      cellIdxDict = g.lonLatMat2cellIdxDict(lonLatMat)
      nPoints = lonLatMat.shape[0]
    else:
      cellIdxDict = locCellIdxDict[0]
      nPoints = locCellIdxDict[1]

    # Convert from cell dict to list of values
    dataValueArry = np.zeros((nPoints,), dtype=np.double)
    # For each key, k, where k is a DggCellIdx
    for k in cellIdxDict.keys():
      # extractIdxs are the lines in lonLatMat that map to each DGG Cell
      extractIdxs = cellIdxDict[k]
      # Get the immediate neighbors
      # ASSUMPTION: radiusKm is < 2*(IntracellDist)
      # FIXME: add radius arg to getCellNeighbors
      nbrIdxs = g.getCellNeighbors(k)
      # If no neighbors are returned then our cell is NaN Poisoned
      if nbrIdxs != []:
        nbrIdxs.append(k)
      # For each line in lonLatMat assoc with DggCellIdx = k
      for i in extractIdxs:
        dataVal = self.getByCellId(i,samp=samp)
        pts = 1
        # For all the cells in the region
        for nbr in nbrIdxs:
          # Check the distance from the lonLatMat point to the neighbor
          # nbrCtr = g.cellDggIdxDict[nbr].center
          distKm = g.cellDistKm
          if distKm <= radiusKm:
            d = self.getByCellId(nbr,samp=samp)
            if not math.isnan(d):
              pts += 1
              dataVal += self.getByCellId(nbr,samp=samp)

        if (dataVal == 0 or len(nbrIdxs) == 0):
          print("no nbrs: key(%d), idx(%d), q2di(%s)" %
                (k, i, str(g.cellDggIdxDict[i].asaTup)))
          nbrIdxs = g.getCellNeighbors(k,debug=True)
        
        # Divide by the number of points
        if pts > 0:
          dataVal /= float(pts)
        # pts == 0 means that we're NaN Poisoned, pass it on
        else:
          dataVal == float('NaN')
          
        dataValueArry[i] = dataVal

    return dataValueArry


  def writeCylProjPng(self, size, filename, region=None, samp = 0, alpha=255):
    # If region is unspecified, assume global
    if region is None:
      region = (-90,90,-180,180)

    grid = self.grid

    # Capture parameters
    (rows, cols) = size
    (minLat, maxLat, minLon, maxLon) = region

    # Determine the height and width in degrees
    hLat = maxLat - minLat
    wLon = maxLon - minLon

    # Determine the height-step and width-step
    stepLat = hLat/float(rows)
    stepLon = wLon/float(cols)

    # Compute the lat and lon coord list
    latArry = minLat + np.arange(0, hLat, stepLat)
    lonArry = minLon + np.arange(0, wLon, stepLon)

    # Create lonLatMat based on the full image size, bounding box, etc.
    lonLatMat = np.zeros((rows * cols, 2),dtype=np.float)
    idx = 0 
    for lat in latArry:
      for lon in lonArry:
        # print idx,lon,lat
        lonLatMat[idx,:] = (lon, lat)
        idx += 1

    # Get the cellIdxDict with grid box indicies associated with the
    # cell idx that they fall into.
    cellIdxDict = grid.lonLatMat2cellIdxDict(lonLatMat)
    
    # Allocate numpy array to make PNG based on output image size
    imgArry = np.zeros((rows,cols,4),dtype=np.uint8)

    # Loop through cell idxs in the dict to associate the rgb of that
    # cell with the places in the image array.  Plug the rgb into the
    # array locations.
    for cellIdx in cellIdxDict.keys():
      (r,g,b) = self.getRgbColorByCellId(cellIdx, samp = samp)
      for idx in cellIdxDict[cellIdx]:
        rImg = idx / cols
        cImg = idx % cols
        imgArry[rImg,cImg,:] = (r,g,b,alpha)

    # Create and save a PNG from the image array
    misc.imsave(filename, imgArry[::-1,...])

  def getDataIdxTupByRegion(self, region):
    dataIdxList = []
    divisorList = []
    for cell in self.grid.cellList:
      if not ((cell.center[0] >= region[0]) and
              (cell.center[0] <= region[1]) and
              (cell.center[1] >= region[2]) and
              (cell.center[1] <= region[3])):
        continue
      cellId  = cell.idx
      (q,i,j) = cell.q2di
      diList = self.cellIdxDict[cellId]

      # dataSum = dataSum + self.data[self.cellIdxDict[cellId],
      #                               self.sampleIdx + samp].mean()
      for dIdx in diList:
        dataIdxList.append(dIdx)
      
        # If the cell is a pent, the area needs adjustment
        if (i == 0) and (j == 0):
          # divisor = divisor + 5.0/6.0
          divisorList.append(5.0/6.0)
        else:
          # divisor = divisor + 1.0
          divisorList.append(1.0)

    return (np.array(dataIdxList), np.array(divisorList, dtype=np.double))


  def getByDataIdxTup(self, dataIdxTup, samp = 0):
    (dataIdxArry, divArry) = dataIdxTup

    dataExt = self.data[dataIdxArry, self.sampleIdx + samp]
    areaAdj = dataExt * divArry

    return areaAdj.mean()
    
  
  # Where region is (minlat, maxlat, minlon, maxlon)
  def getByRegion(self, region, samp = 0):
    # dataSum = 0
    # divisor = 0
    dataIdxList = []
    divisorList = []
    for cell in self.grid.cellList:
      if not ((cell.center[0] >= region[0]) and
              (cell.center[0] <= region[1]) and
              (cell.center[1] >= region[2]) and
              (cell.center[1] <= region[3])):
        continue
      cellId  = cell.idx
      (q,i,j) = cell.q2di
      diList = self.cellIdxDict[cellId]

      # dataSum = dataSum + self.data[self.cellIdxDict[cellId],
      #                               self.sampleIdx + samp].mean()
      for dIdx in diList:
        dataIdxList.append(dIdx)
      
        # If the cell is a pent, the area needs adjustment
        if (i == 0) and (j == 0):
          # divisor = divisor + 5.0/6.0
          divisorList.append(5.0/6.0)
        else:
          # divisor = divisor + 1.0
          divisorList.append(1.0)

    dataExt = self.data[dataIdxList, self.sampleIdx + samp]
    divArry = np.array(divisorList, dtype=np.double)
    areaAdj = dataExt * divArry
    # (dataSum / float(divisor))
    return areaAdj.mean()

            
  def getByCellId(self, cellId, samp = 0):
    if cellId in self.cellIdxDict:
      return self.data[self.cellIdxDict[cellId],
                       self.sampleIdx + samp].mean()
    else:
      # If we can't find data for a cell, poison it.
      return float('NaN')


  def getScaledByCellId(self, cellId, samp = 0):
    if self.ZERO_CENTERED_SCALE:
      return (0.5 + (-1.0 * self.getByCellId(cellId, samp = samp) /
                     (2.0 * max(abs(self.sampleMax),abs(self.sampleMin)))))
    else:
      return ((self.getByCellId(cellId, samp = samp) - self.sampleMin) /
              (self.sampleMax - self.sampleMin))
  

  def getRgbColorByCellId(self, cellId, samp = 0):
    if self.ZERO_CENTERED_SCALE:
      cMapped = cm.PuOr(float(self.getScaledByCellId(cellId, samp = samp)))
    else:
      cMapped = cm.jet(float(self.getScaledByCellId(cellId, samp = samp)))
    r = int(cMapped[0]*255 + 0.5)
    g = int(cMapped[1]*255 + 0.5)
    b = int(cMapped[2]*255 + 0.5)

    return (r,g,b)

  def getKmlColorByCellId(self, cellId, alpha=255, samp = 0):
    if cellId in self.cellIdxDict:
      a = alpha
      (r,g,b) = self.getRgbColorByCellId(cellId, samp = samp)
      
      return "%02x%02x%02x%02x" % (a, b, g, r)

    else:
      return "7f0000ff"

    
  def fromDataMatrix(self, g, data):
    return Data().setDataMatrix(g, data)


  def fromAsaMatrix(self, g, asa, samp = 0, nSamp = 1):
    d8a = Data()

    d8a.dataType             = 'asa'
    d8a.grid                 = g
    d8a.sampleIdx0           = 2
    d8a.sampleIdx            = 2
    d8a.asaSampList          = [ 0 ]

    d8a.asaArrayDictBySample = {}
    d8a.asaArrayDictBySample[0] = asa
    
    d8a.cellIdxDict          = {}
    for i in xrange(g.nCells):
      d8a.cellIdxDict[i] = [i]

    d8a.data = np.zeros((g.nCells, 2 + nSamp), dtype = dtype)
    d8a.data[:, 2 + samp] = array[(g.idxCellAsaMat[:,0],
                                       g.idxCellAsaMat[:,1],
                                       g.idxCellAsaMat[:,2])]
    d8a.RawData = d8a.data

    d8a.characterizeData()

    return d8a


    
# Data Class for intermediate levels in a Pyramid Data Structure    
class PyrLvlData(Data):
  def __init__(self, pyr, grid0, gridN, dataN, samp=None, debug=True):
    if dataN.dataType != 'asa':
      raise TypeError("data layer must be of type 'asa'")

    self.dataType  = 'pyr-lvl'
    self.pyramid   = pyr
    self.grid     = grid0
    self.gridN     = gridN
    self.dataN     = dataN

    self.RawData     = None
    self.data        = np.zeros((grid0.nCells, dataN.data.shape[1]),
                                dtype = dataN.data.dtype)
    self.cellIdxDict = {}
    self.sampleIdx0  = dataN.sampleIdx0
    self.sampleIdx   = dataN.sampleIdx
    self.sampleMin   = dataN.sampleMin
    self.sampleMax   = dataN.sampleMax
    self.sampleMean  = dataN.sampleMean
    
    self.asaSampList          = dataN.asaSampList
    self.asaArrayDictBySample = {}

    g = self.grid
    n = g.resolution
    # h = g.asaHeight
    # w = g.asaWidth

    gN   = self.gridN
    nN   = gN.resolution
    dRes = nN - n

    for i in xrange(g.nCells):
      self.cellIdxDict[i] = [i]
    
    if isinstance(samp, numbers.Integral):
      sampList = [ samp ]
    elif isinstance(samp, list):
      sampList = samp 
    elif len(self.asaSampList) > 0:
      sampList = self.asaSampList
    else:
      sampList = [ self.sampleIdx - self.sampleIdx0 ]
      
    if set(sampList) & set(self.asaSampList) != set(sampList):
      raise TypeError("All samples requested must exist in gridN.")

    # get coarse cell q2di coord list
    q2diMat  = g.idxCellQ2diMat
    # compute coarse cell locations in the fine grid
    q2diMatN = q2diMat * [ 1, 2**dRes, 2**dRes ]
    # convert fine q2di locations to asa
    asaMatN  = gN.mapQ2diMatToAsaMat(q2diMatN)

    # Create Aperature 4 Downsample hex for dRes.  This filter directly
    # maps the coarse hex onto the fine grid.  Most hexels are fully
    # included while some edge hexels are 50% included.
    flt = makeA4DSHexFilter(dRes)
    
    # Loop over samples in Pyramid
    for samp in sampList:
      if debug:
        print("PyrLvlData(): pyramiding sample %d, %d<-%d" % (samp,n,nN)) 
      asaSrc = dataN.asaArrayDictBySample[samp]

      # filter Image with flt
      asaOut = convolveFilter(asaSrc, flt)

      # patch any NaN values in the asaOut (resN, fine) with the
      # application of a child filter
      pyr.patchFilterNaNs(nN, flt, asaSrc, asaOut, q2diMat=q2diMatN,
                          debug=True)

      # extract the coarse resolution locations from the resulting fine
      # grid.
      self.data[:,(samp + self.sampleIdx0)] = \
          asaOut.array[(asaMatN[:,0],asaMatN[:,1],asaMatN[:,2])]

      
class YCSMatData(Data):
  def __init__(self, g):
    # Grid to which data is associated
    self.dataType    = 'simple'
    self.grid        = g
    self.RawData     = None
    self.data        = None
    self.cellIdxDict = None
    self.sampleIdx0  = 2
    self.sampleIdx   = 2
    self.sampleMin   = None
    self.sampleMax   = None
    self.sampleMean  = None
    self.lonLatMat   = None


  def loadLonLatMat(self, filename):
    self.lonLatMat = readLonLatMat(filename)

    
  def loadUnshapedData(self, filename):
    (base, ext) = osp.splitext(osp.basename(filename))
    varname = base.split('_')[0]

    # Raw Data in the form [ lon, lat, dat, var ]
    self.RawData     = readUnshapedSimDat(filename, self.lonLatMat,
                                             matVarName = varname)
    self.data        = self.RawData
    self.sampleIdx0  = 2
    self.sampleIdx   = self.sampleIdx0
    self.cellIdxDict = self.grid.lonLatMat2cellIdxDict(self.data[:,0:2])

    # Check if lon/lat passes sniff test
    if not(isValidLons(self.data[:,0]) and isValidLats(self.data[:,1])):
      raise NameError("Error, file has invalid lon/lat. ")

    self.characterizeData()


  def loadShapedData(self, filename, varname=None,  manyToOne = False):
    (base, ext) = osp.splitext(osp.basename(filename))
    if varname is None:
      varname = base.split('_')[0]

    # Raw Data in the form [ lon, lat, dat, var ]
    self.RawData     = readShapedSimDat(filename, matVarName=varname)
    self.data        = self.RawData
    self.sampleIdx0  = 2
    self.sampleIdx   = self.sampleIdx0
    self.cellIdxDict = self.grid.lonLatMat2cellIdxDict(self.data[:,0:2],
                                                       manyToOne = manyToOne)

    # Check if lon/lat passes sniff test
    if not(isValidLons(self.data[:,0]) and isValidLats(self.data[:,1])):
      raise NameError("Error, file has invalid lon/lat. ")

    self.characterizeData()




class MatSimData(Data):
  def __init__(self, g):
    # Call parent init
    super(MatSimData, self).__init__()
    
    # Grid to which data is associated
    self.grid       = g

    
  def loadFromMat(self, matFilename, goodIdxs=False, matVarName=None,
                  fmtVer = 1):
    matFileDict = scipy.io.loadmat(matFilename, struct_as_record=True)

    if matVarName is None:
      matVarName = osp.splitext(osp.basename(matFilename))[0]

    # Raw data from the matlab file.
    self.rawData   = matFileDict[matVarName]
    # The data is re-shaped/arranged to [lon, lat, samp_1, ..., samp_n] 
    self.data      = None
    # The provided cell idx assumes cells in order, not to be trusted
    self.cellIdx   = xrange(self.rawData.shape[0])

    # fmtVer == 1 - All is well [lon, lat, samp_1, ..., samp_n]
    if fmtVer == 1:
      self.data      = self.rawData
    # fmtVer == 2 - strip off idx [idx, lon, lat, samp_1, ..., samp_n]
    elif fmtVer == 2:
      self.cellIdx   = np.int_(self.rawData[:,0])
      self.data      = self.rawData[:,1:]
    # fmtVer == 3 - strip off idx [idx, lon, lat, samp_1, ..., samp_n]
    # Not sure why this is here...  Same as fmtVer == 2?
    elif fmtVer == 3:
      self.cellIdx   = np.int_(self.rawData[:,0])
      self.data      = np.hstack([self.rawData[:,1:2], self.rawData[:,2:3],
                                  self.rawData[:,3:]])
    # fmtVer == 4 - swap lat/lon  [lat, lon, samp_1, ..., samp_n]
    elif fmtVer == 4:
      self.data      = np.hstack([self.rawData[:,1:2], self.rawData[:,0:1],
                                  self.rawData[:,2:]])
    else:
      self.data      = self.rawData
      
    self.sampleIdx0 = 2
    self.sampleIdx  = self.sampleIdx0

    # Check if lon/lat passes sniff test
    if not(isValidLons(self.data[:,0]) and isValidLats(self.data[:,1])):
      raise NameError("Error, matlab file has invalid lon/lat. " +
                      "Are you using the correct fmtVer?")

    # If we've been given usable indexes, use them
    if goodIdxs and (self.cellIdx is not None):
      self.cellIdxDict = {}
      for i in xrange(len(self.cellIdx)):
        self.cellIdxDict[self.cellIdx[i]] = [ self.data[i,self.sampleIdx] ]
    # Otherwise we need to compute our own from the cell centers...
    else:
      self.cellIdxDict = self.grid.transLonLat2DGGridCell(self.data[:,0:2])

    self.characterizeData()


  def loadFromMVDF(self, filename, manyToOne = False):
    # Raw Data in the form [ lon, lat, dat, var ]
    self.RawData     = readMVDFLonLatDat(filename)
    self.data        = self.RawData
    self.cellIdx     = None
    self.sampleIdx0  = 2
    self.sampleIdx   = self.sampleIdx0
    self.cellIdxDict = self.grid.lonLatMat2cellIdxDict(self.data[:,0:2],
                                                       manyToOne=manyToOne)

    self.characterizeData()

    # Check if lon/lat passes sniff test
    if not(isValidLons(self.data[:,0]) and isValidLats(self.data[:,1])):
      raise NameError("Error, file has invalid lon/lat. ")


  def loadFromMatVecLonLatPair(self, filename, lonLatMat, matVarName = 'YCS'):
    # Raw Data in the form [ lon, lat, dat, var ]
    self.RawData     = readUnshapedSimDat(filename, lonLatMat,
                                             matVarName = matVarName)
    self.data        = self.RawData
    self.cellIdx     = None
    self.sampleIdx0  = 2
    self.sampleIdx   = self.sampleIdx0
    self.cellIdxDict = self.grid.lonLatMat2cellIdxDict(self.data[:,0:2])

    self.characterizeData()

    # Check if lon/lat passes sniff test
    if not(isValidLons(self.data[:,0]) and isValidLats(self.data[:,1])):
      raise NameError("Error, file has invalid lon/lat. ")

    
