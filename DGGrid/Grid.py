# Copyright [2007]. California Institute of Technology.  ALL RIGHTS RESERVED.
# U.S. Government sponsorship acknowledged. Any commercial use must be
# negotiated with the Office of Technology Transfer at the California
# Institute of Technology.

# This software is subject to U. S. export control laws and regulations
# (22 C.F.R. 120-130 and 15 C.F.R. 730-774). To the extent that the
# software is subject to U.S. export control laws and regulations, the
# recipient has the responsibility to obtain export licenses or other
# export authority as may be required before exporting such information
# to foreign countries or providing access to foreign nationals.

# Debugging, if needed...
# import pdb
#
# Log everything, and send it to stderr.
# import logging
# logging.basicConfig(level=logging.DEBUG)

import time
import collections
import warnings

# Import needed scipy/pylab/etc...
import numpy as np

# ASA hex arrays and asa coordinates
from .AsaHexArray import *

# Integration with DGG library
from . import dgglibc

# Get globals
from .config import *

# Get Cross References
from .Cell import Cell
from .vis.GeoTools import *


##############################################################################
# Class: Grid
#
# This class implements a DGG of resolution [0, 14)
##############################################################################
class Grid(object):
  def __init__(self, res):
    # Initialize Instance Variables
    self.gridType      = 'empty' # { empty, lazy, asa, dgg }
    self.resolution    = res
    self.densify       = DENSIFY
    self.nCellsMax     = None
    self.cellAreaKm2   = None
    self.cellDistKm    = None
    self.asaHeight     = None
    self.asaWidth      = None

    self.nCells         = 0
    self.cellList       = []
    self.cellDggIdxDict = collections.OrderedDict()
    self.cellQ2diDict   = collections.OrderedDict()
    self.cellAsaDict    = collections.OrderedDict()

    # Make the data a disconnected object; leaving the comment below as
    # a reminder for completing the refactor.  Will remove (FIXME) when
    # refactored.
    # self.data           = None

    # Cell indexing arrays
    self.nAllocdMatRows = 0
    # Array (2d) of (q, i, j) rows indexed by cell number
    self.idxCellQ2diMat   = None
    # Array (2d) of (a, r, c) rows indexed by cell number
    self.idxCellAsaMat    = None

    # Cell location arrays
    # All arrays containing geo-coords are stored (lon, lat)
    # Array (2d) of (lon, lat) rows indexed by cell Number
    self.idxCellCenterMat = None
    # Array (3d) of cell indexed arrays (2d) of (lon, lat) rows
    # describing the cell hex(or pent)agon containing (6*(densify+1))
    # cells.
    self.idxCellPolyMat   = None

    # Computed grid information based on resolution
    self.nCellsMax   = int(10 * 4**res + 2)
    self.diCoordMax  = int(2**res)
    self.cellAreaKm2 = cellAreaKm2[res]
    self.cellDistKm  = cellDistKm[res]
    self.asaHeight   = int(3 * 2**res + 1)
    self.asaWidth    = int(11 * 2**(res-1) + 1)
    

  def __str__(self):
    lineList = [""]
    lineList.append("Grid before padding:\n")
    unpadDict = {}
    for i in range(self.nCells):
      unpadDict[tuple(self.idxCellAsaMat[i,:])] = self.cellDggIdxDict[i]
      
    for row in range(self.asaHeight):
      a = row % 2
      r = row / 2
      if a == 1:      
        lineList.append("  ")
      for c in range(self.asaWidth):
        if (a,r,c) in unpadDict:
          curCell = unpadDict[(a,r,c)]
          lineList.append("%02i.%i.%i" % curCell.q2diTup)
        else:
          lineList.append(" NaN  ")
      lineList.append("\n")

    lineList.append("\n")
    lineList.append("Grid after padding:\n")

    for row in range(self.asaHeight):
      a = row % 2
      r = row / 2
      if a == 1:      
        lineList.append("  ")
      for c in range(self.asaWidth):
        if (a,r,c) in self.cellAsaDict:
          curCell = self.cellAsaDict[(a,r,c)]
          lineList.append("%02i.%i.%i" % curCell.q2diTup)
        else:
          lineList.append(" NaN  ")
      lineList.append("\n")

    return "  ".join(lineList)

  
  def newFullGrid(self, type = 'lazy', debug=False):
    if self.resolution is not None:
      res = self.resolution
    else:
      print("ERROR: cannot create new grid, resolution is undefined.")
      return

    if type == 'empty':
      self.gridType = 'empty'
      return
      
    # Allocate an array to store the q2di coords of all the cells.
    self.gridType = 'lazy'
    self.idxCellQ2diMat    = np.empty((self.nCellsMax,3), dtype=np.int64)
    self.idxCellQ2diMat[:] = -999
    self.nAllocdMatRows    = self.nCellsMax
    
    # Create all the cells and link them into the grid.
    if debug:
      print(("DGGrid:Grid:newFullGrid(): generating res %d (%s)" %
            (res, type)))
    t00 = time.time()
    t0 = time.time()
    # Make a master list of all coords)
    self.addCells([ (0,0,0) ])
    self.addCells([ (q,i,j)
                      for q in range(1,11)                   
                      for j in range(self.diCoordMax)
                      for i in range(self.diCoordMax) ])
    self.addCells([ (11,0,0) ])
                    
    t1 = time.time()
    if debug:
      print("  Grid generation elapsed wall time: %f" % (t1-t0))

    if (type == 'asa') or (type == 'dgg'):
      # Create the ASA mapping
      self.mapGridToAsa()

    # If we are computing the cell locations and shape, get the center
    # coords and polygons for each cell.
    if type == 'dgg':
      self.setCellGeoData()

    t01 = time.time()
    if debug:
      print("DGGrid:Grid:newFullGrid(): Total wall time: %f" % (t01 - t00))


  # Wouldn't it be nice if we could verify the correctness of a grid?
  # Yeah, I'll write this in my free time.
  def verifyGrid(self):
    print("ERROR: verifyGrid is unimplemented!")
    return None


  # Given a cell index, return its nearest neighbors in ASA space if
  # they exist in the cellAsaDicts.  This quietly silences NaN'd cells
  # that are on the pents or the midlines of each unfolded gore.
  #
  # If depth is specified, the algorithm is called recursively to get
  # neighbors of greater radius from the initial cell.
  def getCellNeighbors(self, cellIdx, depth=1, debug=False):
    if self.gridType == 'empty':    
      (a,r,c) = self.mapQ2diToAsa(self.cellIdxToQ2di(cellIdx))
    else:
      cell = self.cellDggIdxDict[cellIdx]
      (a,r,c) = cell.asaTup

    if debug:
      print("getCellNeighbors(): debugging")
      print("  arc:", (a,r,c))

    if (self.gridType != 'empty' and
        (a,r,c) not in self.cellAsaDict):
      if debug:
        print("  arc not in self.cellAsaDict")
      return []

    neighbors = []
    neighbors.append((1-a, r-(1-a), c+a    ))
    neighbors.append((a,   r,       c+1    ))
    neighbors.append((1-a, r+a,     c+a    ))
    neighbors.append((1-a, r+a,     c-(1-a)))
    neighbors.append((a,   r,       c-1    ))
    neighbors.append((1-a, r-(1-a), c-(1-a)))

    if debug:
      print("  ", neighbors)
      print("  ", [ self.mapAsaToQ2di(arc) for arc in neighbors ])

    nIdxs = []
    for n in neighbors:
      if self.gridType == 'empty':    
        nIdxs.append(self.q2diToCellIdx(self.mapAsaToQ2di(n)))
      else:
        if n in self.cellAsaDict:
          nIdxs.append(self.cellAsaDict[n].idx)

    if depth > 1:
      idxSet = set(nIdxs)
      for idx in nIdxs:
        rNIdx = self.getCellNeighbors(idx, depth=(depth-1), debug=debug)
        idxSet = idxSet | set(rNIdx)
        
      idxSet = idxSet - set([ cellIdx ])
      nIdxs = list(idxSet)
      
    return list(set(nIdxs))


  # This method takes a region (note: lat/lon) and returns a dictionary
  # of all grid cells falling into that region indexed on cellId and
  # containing two tuples, q2di and center lon/lat.
  #
  # Works on gridType: empty
  #
  # This is tuned to work on empty grids, so it can make subsets based
  # on bounding boxes in reasonable time for high resolution grids.
  #
  # For example, at res=13, a region contianing 363849 points (5 deg
  # square at 26 deg latitude) takes 180 seconds (3 minutes).  DO NOT
  # attempt to generate cells over CONUS at res=13, this will blow up in
  # memory.
  #
  # Where region is (minlat, maxlat, minlon, maxlon)
  #   for CONUS, region = (24.396308, 49.384358, -124.848974, -66.885444)
  def cellFloodFillRegion(self, region):
    (minlat, maxlat, minlon, maxlon) = region

    # The seed for the fill is at the center of the bounding box
    seedLonLatMat = np.array([ (minlon + (maxlon-minlon)/2,
                                minlat + (maxlat-minlat)/2) ])

    # Find the seed grid cell id, q2di, and lonLat
    seedQ2diMat = self.lonLatMatToQ2diMat(seedLonLatMat)
    seedIdx     = self.q2diToCellIdx(seedQ2diMat[0,:])
    seedCenter  = self.q2diMatToLonLatMat(seedQ2diMat)

    # Store the found cells, their q2di and lonLat in a dictionary by
    # cell id.  The first cell in the dict is the seed.
    inRegionCellDict = {}
    inRegionCellDict[seedIdx] = (tuple(seedQ2diMat[0,:]),
                                 tuple(seedCenter[0,:]))

    # Initialize the sets for membership: in, out, or examined (in | out)
    inRegionSet  = set([seedIdx])
    outRegionSet = set()
    examinedSet  = set([seedIdx])

    # Initialize the processing queue with the neighbors of the seed
    toProcessQueue = []
    toProcessQueue.extend(set(self.getCellNeighbors(seedIdx)) - examinedSet)

    # While there are cells to consider, loop
    while toProcessQueue:
      # Create parallel lists contining cellId, q2di, and lonLat for all
      # the cells in the current queue.
      idxList    = toProcessQueue
      q2diMat    = np.array([ self.cellIdxToQ2di(idx) for idx in idxList ],
                            dtype=np.uint64)
      lonLatList = list(self.q2diMatToLonLatMat(q2diMat))

      # Clear the queue as we are now processing them
      toProcessQueue = []

      # For each cell indexed in this batch
      for i in range(len(idxList)):
        # Get the cellId
        curIdx    = idxList[i]
        # Check to see if this cell has been examined and skip if so
        if curIdx in examinedSet:
          continue

        # Get the lonLat for checking against the region
        (lon,lat) = lonLatList[i]
        # Add this cell to those examined since we are about to
        examinedSet.add(curIdx)
        # Check for region membership
        if (lon >= minlon and
            lon <= maxlon and
            lat >= minlat and
            lat <= maxlat) :
          # We're in! Add us to the inRegionSet
          inRegionSet.add(curIdx)
          # Store this cell's info in the dict
          inRegionCellDict[curIdx] = (tuple(q2diMat[i,:]), (lon,lat))
          # Add the neighgbors, less any we've examined, to the queue
          toProcessQueue.extend(set(self.getCellNeighbors(curIdx)) -
                                examinedSet)
        else:
          # We're out! Add us to the outRegionSet
          outRegionSet.add(curIdx)

    # Done.  Return the dictionary; hashed on cellId, containing q2di
    # and lonLat.
    return inRegionCellDict
  
  # Generate annd dump, to a string, KML for this grid.  The region, if
  # specified, limits output to that bounding box.
  #
  # Where region is (minlat, maxlat, minlon, maxlon)
  #   for CONUS, region = (24.396308, 49.384358, -124.848974, -66.885444)
  def dumpKml(self, data=None, region=None, outline=False, bluePents=False):
    bufList = []

    # Add Header
    bufList.append(KML_HEADER)
    
    for cell in self.cellList:
      if region is not None:
        if not ((cell.center[0] >= region[0]) and
                (cell.center[0] <= region[1]) and
                (cell.center[1] >= region[2]) and
                (cell.center[1] <= region[3])):
          continue
      bufList.append(cell.dumpKml(data=data, outline=outline,
                                  bluePents=bluePents))

    # Add Footer
    bufList.append(KML_FOOTER)
 
    return "".join(bufList)


  def cellIdxToQ2di(self, idx):
    if idx == 0:
      return (0,0,0)
    if idx == (self.nCellsMax - 1):
      return (11,0,0)
 
    q    = 1 + (idx - 1) / (self.diCoordMax * self.diCoordMax)
    qIdx = (idx - 1)  % (self.diCoordMax * self.diCoordMax)
    i    = qIdx % self.diCoordMax
    j    = qIdx / self.diCoordMax

    return (q,i,j)


  def q2diToCellIdx(self, q2di):
    (q, i, j) = q2di

    if q >= 1 and q <= 10:
      idx = ( 1 + (q - 1) * (self.diCoordMax * self.diCoordMax) +
              j * self.diCoordMax + i )
      return idx
    if q == 0 and i == 0 and j == 0:
      return 0
    if q == 11 and i == 0 and j == 0:
      return (self.nCellsMax - 1)

    
  
  # Given a list of q2di tuples (or a single tuple), create the cells
  # ('lazy') and add them to the grid.
  def addCells(self, q2diSgltnOrList):
    if type(q2diSgltnOrList) == type((0,0,0)):
      q2diList = [ q2diSgltnOrList ]
    else:
      q2diList = q2diSgltnOrList

    nNewCells = len(q2diList)
    if ((nNewCells + self.nCells) > self.nAllocdMatRows):
      print("addCells():ERROR: Insufficient cells allocated in idxCellQ2diMat")
      
    iCell = self.nCells
    for q2di in q2diList:
      newCell = Cell(self, idx=iCell, q2di=q2di)
      self.idxCellQ2diMat[iCell,:] = q2di
      
      self.cellList.append(newCell)
      self.cellDggIdxDict[iCell]   = newCell
      self.cellQ2diDict[q2di]    = newCell
      iCell += 1

    self.nCells = iCell


  # mapGridToAsa(): Create a q2di to ASA mapping for the grid and
  # perform padding.  This also changes the gridType to 'asa'.
  def mapGridToAsa(self, debug=False):
    if self.gridType == 'empty':
      raise TypeError("cannot map to asa, grid is 'empty'") 
    # Update the grid type
    self.gridType = 'asa'

    # Create the mapping from q2di to ASA
    if debug:
      print("DGGrid:Grid:mapGridToAsa() : creating the ASA mapping")
    t0 = time.time()
    self.idxCellAsaMat = self.mapQ2diMatToAsaMat(self.idxCellQ2diMat)
    self.cellAsaDict   = {}
    for i in range(self.nCells):
      self.cellAsaDict[tuple(self.idxCellAsaMat[i,:])] = self.cellDggIdxDict[i]
    t1 = time.time()
    if debug:
      print("  ASA mapping elapsed wall time: %f" % (t1-t0))

    # Pad the grid - create a mapping from ASA coords to the q2di cell
    # across the fold
    if debug:
      print("DGGrid:Grid:mapGridToAsa() : padding grid")
    t0 = time.time()
    self.padGrid()
    t1 = time.time()
    if debug:
      print("  Elapsed wall time: %f" % (t1-t0))


  def q2diMatToLonLatMat(self, q2diMat, debug=False):
    return dgglibc.q2di_to_centers(dggLibCState, self.resolution, q2diMat)
  

  def setCellGeoData(self, debug=False):
    if self.gridType == 'empty':
      raise TypeError("cannot fill in cell data, grid is 'empty'") 

    if debug:
      print("DGGrid:Grid:setCellGeoData(): getting centers and polygons (dgglibc)")
    t0 = time.time()
    q2diMat = self.idxCellQ2diMat
    (centers, polys) = dgglibc.q2di_to_geo_coords(dggLibCState,
                          self.resolution, self.densify, q2diMat)
    self.idxCellCenterMat = centers
    self.idxCellPolyMat   = polys

    for i in range(q2diMat.shape[0]):
      cell = self.cellQ2diDict[tuple(q2diMat[i,:])]
      cell.cellType = 'dgg'
      
    t1 = time.time()
    if debug:
      print("  Elapsed wall time: %f" % (t1-t0))
    

  # Demote a grid to 'lazy' by dumping the centers and polylines from
  # the cells.
  def delCellGeoData(self):
    del self.idxCellCenterMat
    del self.idxCellPolyMat
    self.idxCellCenterMat = None
    self.idxCellPolyMat   = None

    for cell in self.cellList:
      cell.cellType = 'lazy'


  # Given a matrix of lon/lat locations; map them to cell q2di
  # coordinates.
  def lonLatMatToQ2diMat(self, lonLatMat):
    resolution = self.resolution

    q2diMat = dgglibc.lonLatMat_to_q2di(dggLibCState, resolution,
                                        lonLatMat)

    return q2diMat
   
      
  # Given a matrix of lon/lat locations; map them to DGG cells.  The
  # resulting cellIdxDict is a dictionary where the key is the DDGrid
  # cell id and the values are the indexes into the referenced array
  #
  # By default this assumes 1-1 or 1-many (hex-data), however, if the
  # data is more sparse than the hexes, a many-1 linkage may be needed.
  # The manyToOne optional argument enables this more computationally
  # costly option.
  def lonLatMat2cellIdxDict(self, lonLatMat, manyToOne = False):
    resolution = self.resolution

    q2diMat = dgglibc.lonLatMat_to_q2di(dggLibCState, resolution,
                                        lonLatMat)
  
    cellIdxDict = {}
    for i in range(lonLatMat.shape[0]):
      try:
        cellIdxDict[self.cellQ2diDict[tuple(q2diMat[i,:])].idx].append(i)
      except KeyError:
        cellIdxDict[self.cellQ2diDict[tuple(q2diMat[i,:])].idx] = [i]

    if manyToOne:
      knownCellIdxSet = set(cellIdxDict.keys())
      emptyCellIdxs = list(set([ c.idx for c in self.cellList]) -
                           knownCellIdxSet)

      patchedCellIdxDict = {}
      for cellIdx in emptyCellIdxs:
        for depth in [ 2**i for i in range(16) ]:
          neighTestSet = set(self.getCellNeighbors(cellIdx, depth=depth))
          knownCellIdxs = list(knownCellIdxSet & neighTestSet)
          if len(knownCellIdxs) > 0:
            break
        if len(knownCellIdxs) == 0:
          warning.warn(("Unable to fill cell %d in lonLatMat2cellIdxDict() " +
                        "manyToOne") % cellIdx)
          continue

        dataTestIdxs = []
        for idx in knownCellIdxs:
          dataTestIdxs.extend(cellIdxDict[idx])

        center = self.cellDggIdxDict[cellIdx].center
        dist = 40000
        for dIdx in dataTestIdxs:
          testCenter = (lonLatMat[dIdx,1], lonLatMat[dIdx,0])
          testDist = gcDist(center, testCenter)
          if testDist < dist:
            dist = testDist
            patchedCellIdxDict[cellIdx] = [ dIdx ]

      for idx in list(patchedCellIdxDict.keys()):
        cellIdxDict[idx] = patchedCellIdxDict[idx]
        
    return cellIdxDict
  
  
  def q2diIntToTup(self, q2diInt):
    # n = self.resolution

    # Conversion of q2di interleave stored coordinates
    # First 2 places as an integer are the quadrant number 
    q = int(q2diInt[0:2])
    diInt = q2diInt[2:]

    # Next n places are each one bit of the resolution n (i,j)
    # coordinate. So each digit (base 10) can be broken into 2 bits
    # where the first bit belongs to coordinate i and the second to
    # coordinate j.  Since we are processing from MSB to LSB, the p
    # counter in the power to raise 2 in the place
    diList = [ (int(d)/2, int(d)%2) for d in diInt ]
    i = int("".join([str(t[0]) for t in diList ]), 2)
    j = int("".join([str(t[1]) for t in diList ]), 2)
    
    # print 'q2diInt = %s => (%d, %d, %d)' % (q2diInt, q, i, j)
    # print '    %s ' % diInt,
    # print " ".join([bin(int(d)) for d in list(diInt)]),
    # print ' => %s %s' % (bin(i), bin(j))
    
    q2diTup = (q,i,j)
    return q2diTup


  def q2diTupToInt(self, q2diTup):
    (q,i,j) = q2diTup
    diInt   = str(2 * int("{0:b}".format(i)) +
                  int("{0:b}".format(j))).zfill(self.resolution)
    q2diInt = ("%02d%s" % (q, diInt))

    return q2diInt

  
  # Given an ASA coordinate map to q2di across folds, etc. 
  def mapAsaToQ2di(self, xxx_todo_changeme):
    (a,r,c) = xxx_todo_changeme
    n = self.resolution
    asaC  = asa.coord(a,r,c)
    (r2d, c2d) = asaC.as2d
    
    # Determine quadrant from ASA
    # Candidate quadrants
    q0  = 1 + max(min((c / 2**n),4),0)
    q1  = 6 + min(max(((c - 2**(n-1))/2**n),0),4)
    # Cadidate quadrant column origins
    q0o = (q0-1) * 2**n
    q1o = (q1-6) * 2**n + 2**(n-1)
    # Special case q = 0 membership at/above row 0
    if r2d < 0:
      return None
    elif r2d == 0:
      # print "q2di:", (0,0,0)
      return (0,0,0)
    # Special case q = 11 membership at/below row (3 * 2**n)
    elif r2d == 3 * 2**n:
      return (11,0,0)
    elif r2d > 3 * 2**n:
      return None
    # Above or at all q0 origins so that predicts q0
    elif r2d <= 2**n:
      q = q0
    # Below or at all q1 origins so that predicts q1
    elif r2d >= 2**(n+1):
      q = q1
    # In the uncomfortable middle, need to compute more to decide q0 or q1
    else:
      # 2d row offsets from the candidate q0 and q1 origins
      r0off = r2d - 2**n         # Offset DOWN from _q0_ origin
      r1off = -(r2d - 2**(n+1))  # Offset  UP  from _q1_ origin
      # Determine which direction the dividing line runs
      #  diagonal ll to ur
      if q0o < q1o:
        # compute the dividing line
        d = q1o + r1off/2
        if c < d:
          q  = q0
        else:
          q  = q1
      #  diagonal ur to ll
      elif q1o < q0o:
        # compute the dividing line
        d = q0o + r0off/2
        if c < d:
          q  = q1
        else:
          q  = q0
      
    # Compute Quadrant Origin in ASA(a,r,c) space
    ao = 0
    if q < 6:
      ro = 2**(n-1)
      co = q0o
    else:
      ro = 2**n
      co = q1o
    asaO = asa.coord(ao,ro,co)
    
    # Get delta asa coordinate from origin to point.  Use 2d points to
    # simplify row calculations.
    dAsa = asaC - asaO
    (dr2d,dc2d) = dAsa.as2d
     
    # Deterine (i, j) coord from the delta 2d coords
    i = dc2d - (-dr2d)/2
    j = dc2d - dr2d/2

    ret = self.q2diRectify((q,i,j))
    
    return ret
    
  
  def mapQ2diToAsa(self, xxx_todo_changeme1):
    (q,i,j) = xxx_todo_changeme1
    n = self.resolution
    
    # Determine Q2DI quadrant origin in ASA (a,r,c) space 
    a = 0
    if (q == 0):
      r = 0
      c = 2**(n-1)
    elif (q >= 1 and q < 6):
      r = 2**(n-1)
      c = (q-1) * 2**n
    elif (q >= 6 and q < 11):
      r = 2**n
      c = (q-6) * 2**n + 2**(n-1)
    else:
      r = 3 * 2**(n-1)
      c = 2**n

    # Determine ASA offset from Q2DI origin ASA mapping
    if (q > 0  and q < 11):
      a = (a - j + i) % 2
      r = (2*r - j + i) / 2
      c = c + (j + i) / 2

    return (a,r,c)


  # Given a q2diMat with 3 columns (q,i,j) and each row representing a
  # coordinate, map those q2di coords to asa coords using matrix math
  # rather than looping.
  def mapQ2diMatToAsaMat(self, q2diMat):
    n = self.resolution

    # Alloc the Asa Matrix
    asaMat = np.empty(q2diMat.shape, dtype=np.int64)
    asaMat[:] = -999

    # Determine Q2DI quadrant origin in ASA (a,r,c) space 
    # q == 0
    idxs = np.nonzero(q2diMat[:,0] == 0)
    asaMat[idxs, :] = np.zeros(q2diMat[idxs,:].shape,dtype=np.int64)
    asaMat[idxs, 2] = asaMat[idxs, 2] + 2**(n-1)
    
    # 1 <= q <= 5
    idxs = np.nonzero(np.logical_and(q2diMat[:,0] >= 1, q2diMat[:,0] <= 5))
    asaMat[idxs, :] = np.zeros(q2diMat[idxs,:].shape,dtype=np.int64)
    asaMat[idxs, 1] = asaMat[idxs, 1] + 2**(n-1)
    asaMat[idxs, 2] = asaMat[idxs, 2] + (q2diMat[idxs,0]-1) * 2**n

    # 6 <= q <= 10
    idxs = np.nonzero(np.logical_and(q2diMat[:,0] >= 6, q2diMat[:,0] <= 10))
    asaMat[idxs, :] = np.zeros(q2diMat[idxs,:].shape,dtype=np.int64)
    asaMat[idxs, 1] = asaMat[idxs, 1] + 2**n
    asaMat[idxs, 2] = asaMat[idxs, 2] + (q2diMat[idxs,0]-6) * 2**n + 2**(n-1)

    # q == 11
    idxs = np.nonzero(q2diMat[:,0] == 11)
    asaMat[idxs, :] = np.zeros(q2diMat[idxs,:].shape,dtype=np.int64)
    asaMat[idxs, 1] = asaMat[idxs, 1] + (3 * 2**n + 1) / 2
    asaMat[idxs, 2] = asaMat[idxs, 2] + 2**n

    # Determine ASA offset from Q2DI origin ASA mapping
    idxs = np.nonzero(np.logical_and(q2diMat[:,0] >= 1, q2diMat[:,0] <= 10))
    asaMat[idxs, 0] = (asaMat[idxs, 0] -
                       q2diMat[idxs,2] +
                       q2diMat[idxs,1]) % 2
    asaMat[idxs, 1] = (2 * asaMat[idxs, 1] -
                       q2diMat[idxs,2] +
                       q2diMat[idxs,1]) / 2
    asaMat[idxs, 2] = asaMat[idxs, 2] + (q2diMat[idxs,2] +
                                         q2diMat[idxs,1]) / 2
    

    return asaMat

  

  # Pad the grid - create a mapping from ASA coords to the q2di cell
  # across the fold for an unfolding of the ISEA4H icosahedron onto a
  # plane tiled with hexagons
  def padGrid(self, clearPents=False, debug=False):
    n = self.resolution

    padList = []
    mapList = []
    # Create parallel lists, pad and map, that list q2di coords of ASA
    # cells to be padded across the folds and the d2qi coord of the cell
    # value to pad with.
    for q in range(1, 11):
      pl = self.qFwdPaddingRange(q)
      ml = [ self.q2diPaddingMapFwd(i) for i in pl ]
      padList.extend(pl)
      mapList.extend(ml)
      pl = self.qBakPaddingRange(q)
      ml = [ self.q2diPaddingMapBak(i) for i in pl ]
      padList.extend(pl)
      mapList.extend(ml)

    if debug:
      print(("  Padding: nCells: %d, res %d, nPadCells: %d" %
            (self.nCells, n, len(padList))))
      print(("  ASA grid size: h: %d, w: %d, nCells: %d" %
            (self.asaHeight, self.asaWidth,
             self.asaHeight * self.asaWidth)))

    # Convert the pad list to ASA coordinates in order to form the
    # linkage from the ASA dict to the cells that lie across the fold.
    padMat = np.array(padList, dtype=np.int64)    
    asaMat = self.mapQ2diMatToAsaMat(padMat)

    # Now link up the padding by adding all of the mapped cells to the
    # ASA dict in the locations indicated by the pad list (now matrix).
    #
    # print 'linking up padding'
    for i in range(asaMat.shape[0]):
      # print ("  pad: %s, map: %s, asa:%s" %
      #        (padList[i], mapList[i], asaList[i]))
      try:
        self.cellAsaDict[tuple(asaMat[i,:])] = self.cellQ2diDict[mapList[i]]
      except:
        print(("WARNING(linking): Missing dict cells." +
               "  May be due to rerunning padGrid"))
        print(("  pad: %s, map: %s, asa:%s" %
               (padList[i], mapList[i], asaMat[i,:])))

    # As part of padding, I'm clearing out all links to the pentagons.
    # This results in NaNs in the flattening and poisons filters that
    # get too close.
    if clearPents:
      print("clearing pents")
      pents = [ (i,0,0) for i in range(0,12) ]
      # These are the pents that got padded/wrapped
      pents.append([10,0,2**n])
      pents.append([10,2**n,2**n])
      for q2diPent in pents:
        asaPent = self.mapQ2diToAsa(q2diPent)
        try:
          del self.cellAsaDict[asaPent]
        except:
          print(("WARNING(del pent): Missing dict cells." +
                 "  May be due to rerunning padGrid"))
          print(("  pent: %s" % str(asaPent)))

        
  # Given a quadrant, q = [1,10], list all q2di cells forward (to the
  # right, or east) of the quadrant that require padding.  NOTE: these
  # cells are in the unfolded gores and are not normally defined on the
  # DGG.  We get this list in order to determine the cells to be padded
  # and then map them later.
  def qFwdPaddingRange(self,q):
    n = self.resolution

    padList = []
    # Cells in quadrants 1-5 are padded in the upper gores
    if (q < 6):
      i_min = 1
      i_max = 2**n
      for j in range(2**n, 2**n + 2**(n-1)):
        for i in range(i_min, i_max):
          padList.append((q,i,j))
        i_min += 1
        i_max -= 1
    else:
      # Cells in quadrants 6-10 are padded in the lower gores
      # regular case
      j_min = 1
      j_max = 2**n
      for i in range(2**n, 2**n + 2**(n-1)):
        for j in range(j_min, j_max):
          padList.append((q,i,j))
        j_min += 1
        j_max -= 1
      # Cells in quadrand 10 are additionally padded in the middle gore
      # extra corner of quadrant 10
      if (q == 10):
        i_min = 0
        i_max = 2**n + 1
        for j in range(2**n, 2**n + 2**(n-1) + 1):
          for i in range(i_min, i_max):
            padList.append((q,i,j))
          i_min += 1
          i_max -= 1

    return padList


  # Given a quadrant, q = [1,10], list all q2di cells backward (to the
  # left, or west) of the quadrant that require padding.  NOTE: these
  # cells are in the unfolded gores and are not normally defined on the
  # DGG.  We get this list in order to determine the cells to be padded
  # and then map them later.
  def qBakPaddingRange(self,q):
    n = self.resolution
    
    padList = []
    # Cells in quadrants 1-5 are padded in the upper gores
    if (q < 6):
      j_min = 2
      j_max = 2**n - 1
      for i in range(-1, -2**(n-1), -1):
        for j in range(j_min, j_max):
          padList.append((q,i,j))
        j_min += 1
        j_max -= 1
      # Cells in quadrand 1 are additionally padded in the middle gore
      # extra corner of quadrant 1
      if (q == 1):
        i_min = 1
        i_max = 2**n
        for j in range(-1, -2**(n-1) - 1, -1):
          for i in range(i_min, i_max):
            padList.append((q,i,j))
          i_min += 1
          i_max -= 1
    else:
      # Cells in quadrants 6-10 are padded in the lower gores      
      i_min = 2
      i_max = 2**n - 1
      for j in range(-1, -2**(n-1), -1):
        for i in range(i_min, i_max):
          padList.append((q,i,j))
        i_min += 1
        i_max -= 1

    return padList

  
  # Given a q2di cell to be padded forward (east or to the right), map
  # it across the fold.
  def q2diPaddingMapFwd(self, xxx_todo_changeme2):
    (q,i,j) = xxx_todo_changeme2
    n = self.resolution
    
    if (q < 6):
      qp = (q-1 + 1) % 5 + 1
      ip = j - 2**n
      jp = j - i
    else:
      if (q == 10 and j >= 2**n):
        qp = 1
        ip = i
        jp = j - 2**n
        if (i == 2**n and j == 2**n):
          qp = 6
          ip = i - 2**n
      else:
        qp = (q-6 + 1) % 5 + 6
        ip = i - j
        jp = i - 2**n

    return (qp,ip,jp)



  # Given a q2di cell to be padded backward (west or to the left), map
  # it across the fold.
  def q2diPaddingMapBak(self, xxx_todo_changeme3):
    (q,i,j) = xxx_todo_changeme3
    n = self.resolution
    
    if (q < 6):
      if (q == 1 and j < 0):
        qp = 10
        ip = i
        jp = j + 2**n
      else:     
        qp = (q-1 - 1) % 5 + 1
        jp = i + 2**n
        ip = jp - j
    else:
      qp = (q-6 - 1) % 5 + 6
      ip = j + 2**n
      jp = ip - i
      
    return (qp,ip,jp)

  def q2diIsValid(self, xxx_todo_changeme4):
    (q,i,j) = xxx_todo_changeme4
    n = self.resolution

    # Valid q = 0
    if  q == 0 and i == 0 and j == 0:
      return True
    # Valid q = 11
    elif q == 11 and i == 0 and j == 0:
      return True
    # Valid q = [1,10], in main quads
    elif (q >= 1 and q < 11    and
          i >= 0 and i < 2**n  and
          j >= 0 and j < 2**n):
      return True
    else:
      return False


  def q2diRectify(self, qCoord):
    try:
      (q,i,j) = qCoord
    except TypeError:    
      return None
    n = self.resolution

    if self.q2diIsValid((q,i,j)):
      return (q,i,j)
    else:
      # Nothing that we can do for invalid members in quadrant 0 or 11
      if q == 0 or q == 11:
        return None
      # Both i and j being negative implies that we are in the quad
      # behind our current quad.
      if (i < 0 and j < 0):
        # Find quad to the left with cases for upper and lower quads.
        if q <= 5:
          q = 1 + (q-1-1) % 5
        else:
          q = 6 + (q-6-1) % 5
        j = 2**n + j
        i = 2**n + i
      # Points on the i axis in the negative direction
      elif (i < 0 and j == 0):
        # In the upper quads, they are folded onto the current quad
        if q <= 5:
          j = -i
          i = 0
        # In the lower quads, they map to the previous upper quad
        else:
          q = 1 + (q-1-4) % 5
          i = 2**n + i
          j = 0
      # Points on the j axis in the negative direction
      elif (j < 0 and i == 0):
        # In the lower quads, they are folded onto the current quad
        if q >= 6:
          i = -j
          j = 0
        # In the upper quads, they map into the previous lower quad
        else:
          q = 6 + (q-1+4) % 5
          j = 2**n + j
          i = 0
      # Either axis negative implies padding backwards
      elif (i < 0 or j < 0):
        (q,i,j) = self.q2diPaddingMapBak((q,i,j))
      # Either axis in excess of the resolution max implies padding forward
      elif (i >= 2**n or j >= 2**n):
        (q,i,j) = self.q2diPaddingMapFwd((q,i,j))


    if not self.q2diIsValid((q,i,j)):
      # still not valid!?  add more special cases! :P
      return None
        
    return (q,i,j)

### FIXME - Prototype code...
class cellQ2diMat(object):
  def __init__(self, res):
    self.res = res

    # Computed grid information based on resolution
    self.nCellsMax   = 10 * 4**res + 2
    self.diCoordMax  = 2**res

  def __getitem__(self, key):
    try:
      key = tuple(key)
      (cid, coord) = key
    except:
      pass

    return None
