# Copyright [2007]. California Institute of Technology.  ALL RIGHTS RESERVED.
# U.S. Government sponsorship acknowledged. Any commercial use must be
# negotiated with the Office of Technology Transfer at the California
# Institute of Technology.

# This software is subject to U. S. export control laws and regulations
# (22 C.F.R. 120-130 and 15 C.F.R. 730-774). To the extent that the
# software is subject to U.S. export control laws and regulations, the
# recipient has the responsibility to obtain export licenses or other
# export authority as may be required before exporting such information
# to foreign countries or providing access to foreign nationals.

# Debugging, if needed...
# import pdb
#
# Log everything, and send it to stderr.
# import logging
# logging.basicConfig(level=logging.DEBUG)

import warnings
import os.path as osp
import cmath as math
import datetime as dt
import os

import numpy as np
import matplotlib
# ignore the use() warning if we've already chosen a backend elsewhere.
with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    matplotlib.use('agg')
import matplotlib.pyplot as plt

import DGGrid
import ReaderTools as rt
import vis.PolyTools as pt
import vis.GeoTools as gt


## FIXME - Requires Significant Refactoring
def CellByCellOnGridData(dat1, dat2, func):
  g1 = dat1.grid
  g2 = dat2.grid
  if g1.resolution != g2.resolution:
    print "ERROR: grids must be of identical resolution."
    return

  if (g1.nCells != g2.nCells):
    print "ERROR: both grids must have the same number of cells."
    return

  outData = np.ones([g1.nCells, 3]) * float('NaN') 

  for cId in range(g1.nCells):
    # centers are lat/lon and data is lon/lat
    outData[cId, 0] = g1.cellDggIdxDict[cId].center[1] # lon
    outData[cId, 1] = g1.cellDggIdxDict[cId].center[0] # lat
    cell1 = dat1.getByCellId[cId]
    cell2 = dat2.getByCellId[cId]

    outData[cid, 2] = func(cell1, cell2)
    
  dat = DGGrid.MatSimDat(g1)
  
  dat.RawData     = outData
  dat.data        = dat.RawData
  dat.cellIdx     = None
  dat.sampleIdx0  = 2
  dat.sampleIdx   = dat.sampleIdx0
  dat.cellIdxDict = dat.grid.transLonLat2DGGridCell(dat.data[:,0:2])

  dat.characterizeData()

  return dat

## FIXME - Requires Significant Refactoring
def ScoreFused(simDat, fusDat, func):
  g1 = simDat.grid
  g2 = fusDat.grid
  if g1.resolution != g2.resolution:
    print "ERROR: grids must be of identical resolution."
    return

  if (g1.nCells != g2.nCells):
    print "ERROR: both grids must have the same number of cells."
    return

  outData = np.ones([g1.nCells, 3]) * float('NaN') 

  for cId in range(g1.nCells):
    # centers are lat/lon and data is lon/lat
    outData[cId, 0] = g1.cellDggIdxDict[cId+1].center[1] # lon
    outData[cId, 1] = g1.cellDggIdxDict[cId+1].center[0] # lat
    sim = simDat.getByCellId(cId+1)
    fusDat.sampleIdx = 2
    val = fusDat.getByCellId(cId+1)
    fusDat.sampleIdx = 3
    unc = fusDat.getByCellId(cId+1)

    outData[cId, 2] = func(sim, val, unc)
    
  dat = DGGrid.MatSimData(g1)
  
  dat.RawData     = outData
  dat.data        = dat.RawData
  dat.cellIdx     = None
  dat.sampleIdx0  = 2
  dat.sampleIdx   = dat.sampleIdx0
  dat.cellIdxDict = dat.grid.transLonLat2DGGridCell(dat.data[:,0:2])

  dat.characterizeData()

  return dat


def ScoreFuncConf95(sim,val,unc):
  return (1.0 * (abs(sim - val) < unc))


def writeLonLatSamp(g, lonLatMat, filename, samp=0):
  if not(gt.isValidLons(lonLatMat[:,0]) and
         gt.isValidLats(lonLatMat[:,1])):
    raise NameError("Error, lonLatMat seems to have bad lon/lat values. " +
                    "Are they swapped?")
  
  outLines = []
  sampList = g.data.extractDataNearestNeighbor(lonLatMat, samp=samp)
  
  for i in range(len(sampList)):
    outVals = [lonLatMat[i,0],lonLatMat[i,1],sampList[i]]
    outLines.append(",".join([str(val) for val in outVals]))
    
  f = open(filename,'w')
  f.write("\n".join(outLines))
  f.write("\n")
  f.close()
                

def writeLonLatSampArray(lonLatMat, filename, sampArray):
  if not(gt.isValidLons(lonLatMat[:,0]) and
         gt.isValidLats(lonLatMat[:,1])):
    raise NameError("Error, lonLatMat seems to have bad lon/lat values. " +
                    "Are they swapped?")

  goodIdxs = np.nonzero(np.logical_not(np.isnan(sampArray[:,0])))
  
  outArray = np.hstack([lonLatMat[goodIdxs],
                        sampArray[goodIdxs]])

  np.savetxt(filename, outArray, delimiter=',')

  return
 

def getLonLatSampList(g, lonLatMat, sampList):
  if not(gt.isValidLons(lonLatMat[:,0]) and
         gt.isValidLats(lonLatMat[:,1])):
    raise NameError("Error, lonLatMat seems to have bad lon/lat values. " +
                    "Are they swapped?")

  extractedSampList = []
  for samp in sampList:
    extSamples = g.data.extractDataNearestNeighbor(lonLatMat, samp=samp)
    extractedSampList.append(extSamples)

  sampArray = np.array(extractedSampList).transpose()

  return sampArray 


def writeLonLatSampList(g, lonLatMat, filename, sampList, mesErrList = None):
  if not(gt.isValidLons(lonLatMat[:,0]) and
         gt.isValidLats(lonLatMat[:,1])):
    raise NameError("Error, lonLatMat seems to have bad lon/lat values. " +
                    "Are they swapped?")

  outLines = []

  extractedSampList = []
  for samp in sampList:
    extSamples = g.data.extractDataNearestNeighbor(lonLatMat, samp=samp)
    extractedSampList.append(extSamples)

  for i in range(len(extractedSampList[0])):
    outVals =  [lonLatMat[i,0], lonLatMat[i,1]]
    for j in range(len(extractedSampList)):
      outVals.append(extractedSampList[j][i])
      
    outLines.append(",".join([str(val) for val in outVals]))

  f = open(filename,'w')
  f.write("\n".join(outLines))
  f.write("\n")
  f.close()



def writeLonLatSampRadius(g, lonLatMat, radius, filename, samp=0):
  if not(gt.isValidLons(lonLatMat[:,0]) and
         gt.isValidLats(lonLatMat[:,1])):
    raise NameError("Error, lonLatMat seems to have bad lon/lat values. " +
                      "Are they swapped?")
  
  outLines = []
  sampList = g.data.extractDataRadius(lonLatMat, radius, samp=samp)
  
  for i in range(len(sampList)):
    outVals = [lonLatMat[i,0],lonLatMat[i,1],sampList[i]]
    outLines.append(",".join([str(val) for val in outVals]))
    
  f = open(filename,'w')
  f.write("\n".join(outLines))
  f.write("\n")
  f.close()

                

def getLonLatSampRadiusList(g, lonLatMat, radius, sampList):
  if not(gt.isValidLons(lonLatMat[:,0]) and
         gt.isValidLats(lonLatMat[:,1])):
    raise NameError("Error, lonLatMat seems to have bad lon/lat values. " +
                    "Are they swapped?")

  extractedSampList = []
  for samp in sampList:
    extSamples = g.data.extractDataRadius(lonLatMat, radius, samp=samp)
    extractedSampList.append(extSamples)

  sampArray = np.array(extractedSampList).transpose()

  return sampArray


def writeLonLatSampRadiusList(g, lonLatMat, radius, filename, sampList):
  if not(gt.isValidLons(lonLatMat[:,0]) and
         gt.isValidLats(lonLatMat[:,1])):
    raise NameError("Error, lonLatMat seems to have bad lon/lat values. " +
                    "Are they swapped?")

  outLines = []

  extractedSampList = []
  for samp in sampList:
    extSamples = g.data.extractDataRadius(lonLatMat, radius, samp=samp)
    extractedSampList.append(extSamples)

  for i in range(len(extractedSampList[0])):
    outVals =  [lonLatMat[i,0], lonLatMat[i,1]]
    for j in range(len(extractedSampList)):
      outVals.append(extractedSampList[j][i])
      
    outLines.append(",".join([str(val) for val in outVals]))

  f = open(filename,'w')
  f.write("\n".join(outLines))
  f.write("\n")
  f.close()



def dumpKmlForQuad(g, quad):
  bufList = []

  bufList.append(DGGrid.KML_HEADER)

  for cell in g.cellList:
    if cell.q2diTup[0] == quad:
      bufList.append(cell.dumpKml())

  bufList.append(DGGrid.KML_FOOTER)

  return "".join(bufList)


def writeKmlQuadToFile(g, quad, filename):
  open(filename,'w').write(dumpKmlForQuad(g, quad))


def writeKmlToFile(g, filename, region=None):
  open(filename,'w').write(g.dumpKml(region=region))


def writeKmlCellList(g, cellIdList, filename):
  bufList = []

  bufList.append(DGGrid.KML_HEADER)

  for cell in cellIdList:
    bufList.append(cell.dumpKml())

  bufList.append(DGGrid.KML_FOOTER)

  open(filename,'w').write("".join(bufList))


def genClipBoxPolygon(clipBox):

  latMax = clipBox[0]
  latMin = clipBox[1]
  lonMax = clipBox[2]
  lonMin = clipBox[3]

  centerLon = (lonMax - lonMin)/2.0
  centerLat = (latMax - latMin)/2.0
  ctrLonLat = [centerLat, centerLon]

  poly = [ (lonMax, latMax), (lonMax, latMin),
           (lonMin, latMin), (lonMin, latMax),
           (lonMax, latMax) ]

  return ( ctrLonLat, poly )


def writePolyToGenFile(center, poly, filename, id=1):
  outLines = []

  if type(id) != type({}):
    c = center
    center = [ c ]
    p = poly
    poly = [ p ]
    i = id
    id = [ i ]

  for i in range(len(id)):
    outLines.append("%d %f %f" % (id[i], center[i][0], center[i][1]))

    for pt in poly[i]:
      outLines.append(" %f %f" % pt)

    outLines.append("END")

  outLines.append("END")

  f = open(filename,'w')
  f.write("\n".join(outLines))
  f.write("\n")
  f.close()



def writeCentersToFile(g, filename):
  outLines = []

  for cell in g.cellList:
    outLines.append("%f %f" % (cell.center[0], cell.center[1]))

  f = open(filename,'w')
  f.write("\n".join(outLines))
  f.write("\n")
  f.close()



def SaveFigureAsImage(fileName,fig=None,**kwargs):
    ''' Save a Matplotlib figure as an image without borders or frames.
       Args:
            fileName (str): String that ends in .png etc.

            fig (Matplotlib figure instance): figure you want to save as the image
        Keyword Args:
            orig_size (tuple): width, height of the original image used to maintain 
            aspect ratio.
    '''
    fig.patch.set_alpha(0)
    if kwargs.has_key('orig_size'): # Aspect ratio scaling if required
        w,h = kwargs['orig_size']
        fig_size = fig.get_size_inches()
        w2,h2 = fig_size[0],fig_size[1]
        fig.set_size_inches([(w2/w)*w,(w2/w)*h])
        fig.set_dpi((w2/w)*fig.get_dpi())
    a=fig.gca()
    a.set_frame_on(False)
    a.set_xticks([]); a.set_yticks([])
    plt.axis('off')
    # plt.xlim(0,h); plt.ylim(w,0)
    fig.savefig(fileName, transparent=True, bbox_inches='tight', \
        pad_inches=0)


# Testing and Diagnosis Functions #
###################################
def writeKmlDiagBins(g, diag, fileBase):
  binDict = diag[0]
  keyList = sorted(binDict.keys())

  for key in keyList:
    print "Bin %s processing..." % str(key)
    iKey = int(key)
    cells = [ g.cellDggIdxDict[i] for i in binDict[key] ]
    writeKmlCellList(g, cells, '%s-%03d.kml' % (fileBase, iKey))
    print "  done."


def diagnoseData(g):
  d = g.data

  nItemsDict = {}
  for i in d.cellIdxDict.keys():
    nItems = len(d.cellIdxDict[i])
    if nItems in nItemsDict:
      nItemsDict[nItems].append(i)
    else:
      nItemsDict[nItems] = [ i ]

  totalItems = 0
  for key in sorted(nItemsDict.keys()):
    totalItems += int(key) * len(nItemsDict[key])

  return (nItemsDict, totalItems)
