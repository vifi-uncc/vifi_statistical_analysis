# Copyright [2007]. California Institute of Technology.  ALL RIGHTS RESERVED.
# U.S. Government sponsorship acknowledged. Any commercial use must be
# negotiated with the Office of Technology Transfer at the California
# Institute of Technology.

# This software is subject to U. S. export control laws and regulations
# (22 C.F.R. 120-130 and 15 C.F.R. 730-774). To the extent that the
# software is subject to U.S. export control laws and regulations, the
# recipient has the responsibility to obtain export licenses or other
# export authority as may be required before exporting such information
# to foreign countries or providing access to foreign nationals.

# Debugging, if needed...
# import pdb
# 
# Log everything, and send it to stderr.
# import logging
# logging.basicConfig(level=logging.DEBUG)

# import sys
import collections

from .AsaHexArray import *

# Import needed objects from other modules
from .Grid import *
from .Data import *

# Get globals
from .config import *

##############################################################################
# Class: Pyramid
#
# This class implements a pyramid of DGGs
##############################################################################
class Pyramid(object):
  def __init__(self, res0=3, resF=8, resStep=3):
    self.res0    = res0
    self.resF    = resF
    self.resStep = resStep
    self.resList = range(res0,resF,resStep) + [resF]
    
    self.gridList    = None
    self.gridResDict = None

    self.dataList    = None
    self.dataResDict = None

    # Dict addressed  [gridRes, childRes, q, i, j]
    self.q2diGridChildDict = {}
    

  # Given the base (largest, finest resolution) grid for which we can
  # assign a data layer, generate the intermediate pyramid grid levels,
  # and link up the children from each coarser layer to the finer layer
  # below.
  def initGrid(self, grid, debug=False):
    self.gridList = []
    self.gridResDict = collections.OrderedDict()
    for res in xrange(self.res0, self.resF, self.resStep):
      if debug:
        print("initGrid(): creating grid(%s)" % res)
      g = Grid(res)
      g.newFullGrid(type='dgg')
      self.gridList.append(g)
      self.gridResDict[res] = g

    if self.resF != grid.resolution:
      raise TypeError("grid resolution (%d) must match pyramid max res (%d)" %
                      (grid.resolution, self.resF))
    if debug:
      print("initGrid(): linking in grid(%s)" % grid.resolution)
    self.gridList.append(grid)
    self.gridResDict[grid.resolution] = grid

    for idx in xrange(len(self.resList) - 1):
      g0 = self.gridList[idx]
      gN = self.gridList[idx+1]
      if debug:
        print("initGrid(): calling self.setChildren(%d,%d)" %
              (g0.resolution, gN.resolution))
      self.setChildren(g0.resolution, gN.resolution) 


  def initData(self, data, debug=False):
    if debug:
      print("initData(): Initializing data...")

    resList = range(self.res0, self.resF, self.resStep)
    resList.reverse()
    resNext = self.resF
    if debug:
      print("initData(): resList: %s" % str(resList))

    data.makeAsaArrays()  
    self.dataList = [ data ]
    self.dataResDict = {}
    self.dataResDict[self.resF] = data
    for res in resList:
      if debug:
        print("initData(): Downsampling %d to %d..." % (resNext, res))
      dataRef = PyrLvlData(self, self.gridResDict[res],
                           self.gridResDict[resNext],
                           self.dataResDict[resNext])
      dataRef.makeAsaArrays()
      if debug:
        print("initData(): Done.")
      self.dataList.insert(0,dataRef)
      self.dataResDict[res] = dataRef
      resNext = res
      

  # Assuming that the different resolution grids references with res0
  # (coarser) and resN (finer) exist within the Pyramid class
  # instantiation, generate links from all cells in grid0 to their
  # children in gridN below and store those links in the
  def setChildren(self, res0, resN, debug=False):
    grid0 = self.gridResDict[res0]
    gridN = self.gridResDict[resN]
    resD = resN - res0

    flt = makeA4DSHexFilter(resD)
    ctr = flt.center
    asaFltArry = [ coord(fCoord) - ctr
                   for fCoord in np.vstack(np.nonzero(flt.array)).transpose() ]

    if debug:
      print("asaFltArry:", [ tuple(aCoord) for aCoord in asaFltArry ])
    
    # For all cells in grid0, determine children in gridN.
    for idx in xrange(grid0.nCells):
      cell = grid0.cellList[idx]
      q2di0 = cell.q2diTup
      if debug:
        print("q2di0:", q2di0)
      (q,i,j) = q2di0
      q2diN   = (q, i*(2**resD), j*(2**resD))
      if debug:
        print("q2diN:", q2diN)

      # Get the ASA location of the center child in the finer grid.
      asaN    = coord(gridN.mapQ2diToAsa(q2diN))
      if debug:
        print("asaN:", asaN)
      # Compute the ASA coords of the children in the finer grid
      # relative to the center child.
      gridNAsaCoords = [ tuple((fCoord + asaN).coord)
                         for fCoord in asaFltArry ]
      if debug:
        print("gridNAsaCoords:", gridNAsaCoords)

      # Now map these points into q2di coordinates taking into account
      # folding, etc. These should be the child points, EXCEPT they are
      # broken for q=0 and q=11.
      gridNq2diCoords = [ gridN.mapAsaToQ2di(aCoord)
                          for aCoord in gridNAsaCoords ]
      if debug:
        print("\ngridNq2diCoords:", gridNq2diCoords)

      # Now we can fix those broken q=0 and q=11 points.  They are not
      # completely broken.  For the upper pole, the results in q=1 are
      # ok.  For the lower pole, q=6 is ok.  The strategy is to pull out
      # the correct quadrant and dupe it for all the quads in that
      # hemisphere.  All quadrants are rotationally symmetric about the
      # pole WRT the downsample filter, which makes this easy.
      
      q2diCoords = gridNq2diCoords
      # North Pole case
      if   q == 0:
        # pull quadrant 1
        q1Children = [ qCoord for qCoord in gridNq2diCoords 
                       if (qCoord is not None) and (qCoord[0] == 1) ]
        if debug:
          print("\nq1Children:       %s" % str(q1Children))
        # dup quad 1 around to all the other quads
        q2diCoords = [ (qNew,qCoord[1],qCoord[2])
                       for qCoord in q1Children
                       for qNew in xrange(1,6) ]
        # don't forget to add the pole
        q2diCoords.append(q2diN)
        if debug:
          print("\nRe-Rectified:     %s" % str(gridNq2diCoords))
          
      # South Pole case
      elif q == 11:
        # pull quadrant 6
        q6Children = [ qCoord for qCoord in gridNq2diCoords 
                       if (qCoord is not None) and (qCoord[0] == 6) ]
        if debug:
          print("\nq6Children:       %s" % str(q6Children))
        # dup quad 6 around to all the other quads
        q2diCoords = [ (qNew,qCoord[1],qCoord[2])
                       for qCoord in q6Children
                       for qNew in xrange(6,11) ] 
        # don't forget to add the pole
        q2diCoords.append(q2diN)
        if debug:
          print("\nRe-Rectified:     %s" % str(gridNq2diCoords))

      # Due to folding, some coordinates are duplicated.  Running the
      # list through the set() type effectively makes sure we have
      # unique coords only.
      uniqueQ2diCoords = list(set(q2diCoords))
      # Sort for human readability.
      uniqueQ2diCoords.sort()
 
      self.q2diGridChildDict[res0,resN,q,i,j] = uniqueQ2diCoords

      if debug:
        print("\nDict[%d,%d,%d,%d,%d]: %s" %
              (res0, resN, q, i, j,
               str(self.q2diGridChildDict[res0,resN,q,i,j])))

  # Given a coarse grid, res, and target coord, q2di0, compute the
  # child filter, flt, response in the fine grid, resN.
  def applyDictFilter(self, res, q2di, flt, asaArray, debug=False):
    grid = self.gridResDict[res]

    # ctr = flt.center
    asaFltList = flt.dict.keys()
    asaFltList.sort()
    valFltList = [ flt.dict[fCoord] for fCoord in asaFltList ]
    fltSize = len(asaFltList)

    if debug:
      print("q2di:", q2di)
    (q,i,j) = q2di

    # Get the ASA location of the center child grid.
    asaCoord = coord(grid.mapQ2diToAsa(q2di))
    if debug:
      print("asa:", asaCoord)
    # Compute the ASA coords of the children in the finer grid
    # relative to the center child.
    gridAsaCoords = [ tuple((fCoord + asaCoord).coord)
                      for fCoord in asaFltList ]
    if debug:
      print("gridAsaCoords:", gridAsaCoords)

    # Now map these points into q2di coordinates taking into account
    # folding, etc. These should be the child points, EXCEPT they are
    # broken for q=0 and q=11.
    gridQ2diCoords = [ grid.mapAsaToQ2di(aCoord)
                       for aCoord in gridAsaCoords ]
    if debug:
      print("\ngridQ2diCoords:", gridQ2diCoords)

    # Now we can fix those broken q=0 and q=11 points.  They are not
    # completely broken.  For the upper pole, the results in q=1 are
    # ok.  For the lower pole, q=6 is ok.  The strategy is to pull out
    # the correct quadrant and dupe it for all the quads in that
    # hemisphere.  All quadrants are rotationally symmetric about the
    # pole WRT the downsample filter, which makes this easy.
    q2diCoords = gridQ2diCoords
    qIdxs = [ gridQ2diCoords.index(qCoord) for qCoord in q2diCoords ]
    # North Pole case
    if   q == 0:
      # pull quadrant 1
      q1Children = [ qCoord for qCoord in gridQ2diCoords 
                     if (qCoord is not None) and (qCoord[0] == 1) ]
      if debug:
        print("\nq1Children:       %s" % str(q1Children))
      # dup quad 1 around to all the other quads
      q2diCoords = [ (qNew,qCoord[1],qCoord[2])
                     for qCoord in q1Children
                     for qNew in xrange(1,6) ]
      qIdxs = [ gridQ2diCoords.index(qCoord)
                for qCoord in q1Children
                for qNew in xrange(1,6) ]
      # don't forget to add the pole
      q2diCoords.append(q2di)
      qIdxs.append(gridQ2diCoords.index(q2di))
          
    # South Pole case
    elif q == 11:
      # pull quadrant 6
      q6Children = [ qCoord for qCoord in gridQ2diCoords 
                     if (qCoord is not None) and (qCoord[0] == 6) ]
      if debug:       
        print("\nq6Children:       %s" % str(q6Children))
      # dup quad 6 around to all the other quads
      q2diCoords = [ (qNew,qCoord[1],qCoord[2])
                     for qCoord in q6Children
                     for qNew in xrange(6,11) ] 
      qIdxs = [ gridQ2diCoords.index(qCoord)
                for qCoord in q6Children
                for qNew in xrange(1,6) ]
      # don't forget to add the pole
      q2diCoords.append(q2di)
      qIdxs.append(gridQ2diCoords.index(q2di))

    qVals = [ valFltList[idx] for idx in qIdxs ]
    norm  = 1.0 / sum(qVals)

    resp = 0
    nVals = 0
    for idx in xrange(len(q2diCoords)):
      if q2diCoords[idx] is None:
        continue
      datum = asaArray[grid.cellQ2diDict[tuple(q2diCoords[idx])].asaTup]
      if debug and np.isnan(datum):
        print("coord: %s is NaN" % str(q2diCoords[idx]))
      nVals += 1
      resp += qVals[idx] * datum

    if nVals < fltSize:
      resp *= norm
      
    return resp

  # Given the coarse resolution, res0, the fine resolution, resN, a
  # filter to process resN -> res0, and the output asaArray at fine
  # scale, patch NaNs in the image by using the child filter
  def patchFilterNaNs(self, res, flt, asaSrc, asaOut, q2diMat=None,
                      debug=False):
    # Working with thefiner grid, grid0
    grid = self.gridResDict[res]

    # Pull the list of valid ASA coords and matching cell indexes 
    if q2diMat is None:
      q2diMat = grid.idxCellQ2diMat
      asaMat  = grid.idxCellAsaMat
    else:
      asaMat  = grid.mapQ2diMatToAsaMat(q2diMat)

    # Separate asa coords into arrays of A, R, and C
    asaMatA = asaMat[:,0]
    asaMatR = asaMat[:,1]
    asaMatC = asaMat[:,2]

    # Pull out the indexes of all valid asa cells that are NaN'd
    nanAsaIdx = np.nonzero(
      np.isnan(asaOut.array[asaMatA,asaMatR,asaMatC]))[0]

    if debug:
      print("patchFilterNaNs(): patching %d NaNs" % len(nanAsaIdx))
    
    for idx in nanAsaIdx:
      q2di = tuple(q2diMat[idx,:])
      val = self.applyDictFilter(res, q2di, flt, asaSrc)
      asaOut.array[asaMatA[idx],asaMatR[idx],asaMatC[idx]] = val

    return
  
  def writeChildrenKml(self, res0, q2di0, resN, outDir,
                       kmlRegInfo=(10,120,5,5), debug=False):
    (q,i,j) = q2di0
    filename = ("children-of-%d-at-%d-%d-%d-in-%d.kml" %
                (res0,q,i,j,resN))
    if osp.exists(outDir + '/' + filename):
      if debug:
        print("skipping %s, exists" % filename)
      return
    
    # g     = self.gridResDict[res0]
    gN    = self.gridResDict[resN]
    dataN = self.dataResDict[resN]
    (minLodPix, maxLodPix, minFade, maxFade) = kmlRegInfo

    bufList = []
    childList = self.q2diGridChildDict[res0,resN,q,i,j]
    bufList.append(KML_HEADER)
    for cQ2di in childList:
      buf = gN.cellQ2diDict[cQ2di].dumpKml(data = dataN,
                                           kmlRegInfo=kmlRegInfo)
      bufList.append(buf)
      if resN != self.resF:
        resIdx = self.resList.index(resN)
        resNN  = self.resList[resIdx+1]
        
        (minLon, minLat, maxLon, maxLat) = gN.cellQ2diDict[cQ2di].getLonLatBox
        d = {}
        d['minLon'] = minLon
        d['minLat'] = minLat
        d['maxLon'] = maxLon
        d['maxLat'] = maxLat
        d['minLodPix'] = minLodPix
        d['maxLodPix'] = maxLodPix
        d['minFade'] = minFade
        d['maxFade'] = maxFade
        kmlRegionStr = KML_REGION % d

        (cq,ci,cj) = cQ2di
        subFilename = ("children-of-%d-at-%d-%d-%d-in-%d.kml" %
                       (resN,cq,ci,cj,resNN))

        d = {}
        d['name']      = subFilename
        d['relPath']   = './' + subFilename
        d['kmlRegion'] = kmlRegionStr
        bufList.append(KML_NETWORKLINK % d)

        self.writeChildrenKml(resN, cQ2di, resNN, outDir,
                              kmlRegInfo=kmlRegInfo)
        
    bufList.append(KML_FOOTER)
    f = file(outDir + "/" + filename, 'w')
    if debug:
      print("writing %s " % filename)
    f.write("".join(bufList))
    f.close()
        
  def writeKml(self,outDir, kmlRegInfo=(150,150*8,8,8), debug=False):
    filename = "doc.kml"
    if osp.exists(outDir + '/' + filename):
      if debug:
        print("skipping %s, exists" % filename)
      return
    
    res0 = self.resList[0]
    g    = self.gridResDict[res0]
    data = self.dataResDict[res0]
    (minLodPix, maxLodPix, minFade, maxFade) = kmlRegInfo

    bufList = []
    childList = [ tuple(q) for q in g.idxCellQ2diMat ]
    bufList.append(KML_HEADER)
    for cQ2di in childList:
      buf = g.cellQ2diDict[cQ2di].dumpKml(data=data,
                                          kmlRegInfo=kmlRegInfo)
      bufList.append(buf)
      if res0 != self.resF:
        resIdx = self.resList.index(res0)
        resN   = self.resList[resIdx+1]
        
        (minLon, minLat, maxLon, maxLat) = g.cellQ2diDict[cQ2di].getLonLatBox
        d = {}
        d['minLon'] = minLon
        d['minLat'] = minLat
        d['maxLon'] = maxLon
        d['maxLat'] = maxLat
        d['minLodPix'] = minLodPix
        d['maxLodPix'] = maxLodPix
        d['minFade'] = minFade
        d['maxFade'] = maxFade
        kmlRegionStr = KML_REGION % d

        (cq,ci,cj) = cQ2di
        subFilename = ("children-of-%d-at-%d-%d-%d-in-%d.kml" %
                       (res0,cq,ci,cj,resN))

        d = {}
        d['name']      = subFilename
        d['relPath']   = './' + subFilename
        d['kmlRegion'] = kmlRegionStr
        bufList.append(KML_NETWORKLINK % d)

        self.writeChildrenKml(res0, cQ2di, resN, outDir,
                              kmlRegInfo=kmlRegInfo)
        
    bufList.append(KML_FOOTER)
    f = file(outDir + "/" + filename, 'w')
    if debug:
      print("writing %s " % filename)
    f.write("".join(bufList))
    f.close()
