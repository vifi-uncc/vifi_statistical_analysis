# Copyright [2007]. California Institute of Technology.  ALL RIGHTS RESERVED.
# U.S. Government sponsorship acknowledged. Any commercial use must be
# negotiated with the Office of Technology Transfer at the California
# Institute of Technology.

# This software is subject to U. S. export control laws and regulations
# (22 C.F.R. 120-130 and 15 C.F.R. 730-774). To the extent that the
# software is subject to U.S. export control laws and regulations, the
# recipient has the responsibility to obtain export licenses or other
# export authority as may be required before exporting such information
# to foreign countries or providing access to foreign nationals.

# import os
# import os.path as osp
import sys
import getopt

import numpy as np
import scipy.io as scio

import DGGrid


def readAirsLocations(inputFilename, filter=None, debug=False):
  f = open(inputFilename, 'r')

  # Get out header names and determine name/idx mapping
  headers = f.readline().split()
  headIdxDict = {}
  for i in range(len(headers)):
    headIdxDict[headers[i]] = i

  # Process each file line
  locations = []
  for line in f:
    toks = [ float(t.strip()) for t in line.split() ]
    if filter is not None:
      if ((toks[headIdxDict['month']] != 4) or
          (toks[headIdxDict['day']] > 3)):
        continue
    locations.append((toks[headIdxDict['lon']], toks[headIdxDict['lat']]))

  if debug:
    print (locations[-20:])
    for i in range(len(headers)):
      print ("%s = %s" % (headers[i], toks[i]))

  # Return locations as a numpy array of [ (lon, lat) ]
  return np.array(locations)



def readOCO2Locations(inputFilename):
  f = open(inputFilename, 'r')

  # Process each file line
  locations = []
  for line in f:
    toks = [ float(t.strip()) for t in line.split(',') ]
    locations.append((toks[0], toks[1]))

  # Return locations as a numpy array of [ (lon, lat) ]
  return np.array(locations)
  


# CSV Locations includes [ lon, lat, day ]
#   where day can be:
#     {'day in the repeat cycle', or 'day of year'}
def readCSVLocationsToDict(inputFilename):
  f = open(inputFilename, 'r')

  # Process each file line
  locDictByDay = {}
  for line in f:
    toks = [ float(t.strip()) for t in line.split(',') ]
    lon = toks[0]
    lat = toks[1]
    day = toks[2]

    try:
      locDictByDay[day].append((lon, lat, day))
    except KeyError:
      locDictByDay[day] = []
      locDictByDay[day].append((lon, lat, day))
    
  locMatDictByDay = {}
  for day in locDictByDay.keys():
    locMatDictByDay[day] = np.array(locDictByDay[day])

  # Return locations as a numpy array of [ (lon, lat) ]
  return locMatDictByDay
  


def readMVDFLonLatDat(inputFilename):
  f = open(inputFilename, 'r')

  # Process each file line
  data = []
  for line in f:
    toks = [ float(t.strip()) for t in line.split(',') ]
    datum = toks # [ toks[0], toks[1], toks[2], toks[3] ]
    data.append(datum)

  # Return locations as a numpy array of [ (lon, lat) ]
  return np.array(data)



def readLonLatMat(lonLatMatFilename):
  matFileDict = scio.loadmat(lonLatMatFilename, struct_as_record=True)

  return matFileDict['LonLatFull']



def readShapedSimDat(matSimDatFilename, matVarName = 'YCS'):
  matFileDict = scio.loadmat(matSimDatFilename,
                                 struct_as_record=True)
  ycs = matFileDict[matVarName]
  return ycs



def readUnshapedSimDat(matSimDatFilename, lonLatMat, matVarName = 'YCS'):
  matFileDict = scio.loadmat(matSimDatFilename, struct_as_record=True)

  nData = lonLatMat.shape[0]
  ycs = matFileDict[matVarName].reshape((-1, nData)).transpose()

  return np.hstack((lonLatMat, ycs))



def readUnshapedSimBlock(matSimDatList, lonLatMat, grid, matVarName = 'YCS'):

  nFiles = len(matSimDatList)
  sumArr = None
  for file in matSimDatList:
    inArr = readUnshapedSimDat(file, lonLatMat, matVarName = 'YCS')
    if sumArr is None:
      sumArr = inArr[:,2:]
    else:
      sumArr = sumArr + inArr[:,2:]

  sim_block = np.hstack([ lonLatMat, (sumArr / float(nFiles)) ])

  d8a = DGGrid.MatSimData(grid)
  d8a.RawData = sim_block
  d8a.data = sim_block
  d8a.cellIdx = None
  d8a.sampleIdx0 = 2
  d8a.sampleIdx = 2
  d8a.cellIdxDict = grid.transLonLat2DGGridCell(d8a.data[:,0:2])
  d8a.characterizeData()

  return d8a

def usage():
  pass

def test():
  try:
    opts, args = getopt.getopt(sys.argv[1:], "2ho:v", ["help", "output="])
  except getopt.GetoptError:
    # print help information and exit:
    usage()
    sys.exit(2)
  output = None
  verbose = False
  OCO2 = False
  for o, a in opts:
    if o == "-v":
      verbose = True
    if o == "-2":
      OCO2 = True
    if o in ("-h", "--help"):
      usage()
      sys.exit()
    if o in ("-o", "--output"):
      output = a
  # Remaining arguments are stored in: args (0 indexed)
  inputFilename = args[0]

  if not OCO2:
    locations = readAirsLocations(inputFilename)
  elif OCO2:
    locations = readOCO2Locations(inputFilename)

  print (locations[-20:])
    
if (__name__ == "__main__"):
  print ('executing test()...')
  test()
  print ('completed test()')
