# -*- coding: utf-8 -*-
"""
Created on Mon Apr 27 17:05:38 2015

@author: marchett
"""


import scipy as sp
from .basisFunction_setup import *
from collections import namedtuple


class basisFunction(object):
  def __init__(self, position = 'regularHex'):
    # Initialize Instance Variables
    self.type      = 'bisquare'
    self.position  = position # { regularHex }
    self.centers   = None
    self.distance  = None
    self.aperture  = None
    
    
  def basisCenters(self, levels, position = 'regularHex'):
    if self.position == 'regularHex':
      self.centers = centersRegularHex(levels)
      self.aperture = sp.unique(self.centers[:, 2])
    else:
      print('only regularHex implemented')
      return


  def deleteBasis(self, S, threshold = 0.2, nw = 10):
    r = S.shape[1]
    wsig = sp.zeros(r)
    for j in range(r):
      weights = sp.sparse.find(S[:, j])[2]
      if(len(weights) < nw):
        wsig[j] = 1
      else:
        wlength = len(sp.where(weights <= threshold)[0])
        wsig[j] = wlength / float(len(weights))
      
    basiskeep = sp.where(wsig < 0.5)[0]
    S = S[:, basiskeep]
    self.centers = self.centers[basiskeep, :]   

    return S
  
  
  def bisquare(self, coords, block = 50000.):
    datasize = len(coords)
    r = len(self.centers)
    last_ind = 0
    i_x = [0] * int(datasize * r * .1)
    i_y = [0] * int(datasize * r * .1)
    i_z = [0] * int(datasize * r * .1)
    
    for i in range(int(sp.ceil(datasize / block))):
        start_ind = int(i * block)
        stop_ind  = (i + 1) * block
        stop_ind = int(min(stop_ind, datasize))
        coords_block = coords[start_ind:stop_ind, :]
        
        dist = sdist(coords_block, self.centers)
        cc = sp.array([self.centers[:, 2], ] * len(coords_block))
        S = 1 - (dist/cc)**2
        S[S < 0] = 0
        S = sp.sparse.csr_matrix(S)
        S = S.multiply(S)
        
        inds = sp.sparse.find(S)
        vect_length = len(inds[0])
        first_ind = last_ind
        last_ind  = first_ind + vect_length
        
        i_x[first_ind:last_ind] = inds[0] + start_ind
        i_y[first_ind:last_ind] = inds[1]
        i_z[first_ind:last_ind] = inds[2]
        
    i_x = i_x[0:last_ind]
    i_y = i_y[0:last_ind]
    i_z = i_z[0:last_ind]
    
    S = sp.sparse.csr_matrix((i_z, (i_x, i_y)), shape=(datasize, r))
    
    return S
  
  
  def bisquare_sp(self, coords):
    
    S = sdist_sp(coords, self.centers)
                
    return S

  def Snot_sp(self, win, locations):
    #result = landMask(win, locations)
    #locations = locations[- result, :]
    predlocs = locations[sp.lexsort(locations.T)]
    if self.type == 'bisquare':
      S0 = self.bisquare_sp(predlocs)
    else:
      print('ERROR: only bisquare basis functions implemented.')
          
    point = namedtuple('Point', ['S0', 'predlocs'])
    output = point(S0, predlocs)
  
    return output
  
  
  

    
