# -*- coding: utf-8 -*-
"""
@author: maggiej
"""

import scipy as sp
from .Grid import *
# from readerTools import landMask



# centers on a regular hex grid
def centersRegularHex(levels):
  centers = sp.empty((0, 3), int)
  for l in range(len(levels)):
    g = Grid(levels[l])
    g.newFullGrid(type='dgg', debug=True)
    aper = g.cellDistKm

    latlon = g.idxCellCenterMat
    aper = sp.repeat(aper * 1.5, len(latlon))
    centers = sp.append(centers, sp.c_[latlon, aper], axis = 0)

  # result = landMask(win=(-90, 90, -180, 180), locations = centers) #no need to landmask
  # centers = centers[- result, :]

  return centers


# spherical distance
def sdist(x, y):
    if(x.ndim == 1):
        x = x.reshape(1,len(x))
    if(y.ndim == 1):
        y = y.reshape(1,len(y))
    R = 6371.0087714
    to_radians = sp.pi/180
    x = x*to_radians
    y = y*to_radians
    d = sp.zeros(shape=(x.shape[0], y.shape[0]))
    for i in range(0, len(y)):
        a = sp.sin(x[:, 1])*sp.sin(y[i, 1])
        b = sp.cos(x[:, 1])*sp.cos(y[i, 1])*sp.cos(x[:, 0] - y[i, 0])
        c = sp.arccos(a + b)
        d[:, i] = R * c

    return d


def sdist_sp(x, y):
    if(x.ndim == 1):
        x = x.reshape(1,len(x))
    if(y.ndim == 1):
        y = y.reshape(1,len(y))
    R = 6371.0087714
    to_radians = sp.pi/180
    x = x*to_radians
    y1 = y*to_radians
    data = []
    row_ind = []
    col_ind = []
    #d = sp.sparse.csr_matrix((x.shape[0], y.shape[0]))
    for i in range(0, len(y1)):
        a = sp.sin(x[:, 1])*sp.sin(y1[i, 1])
        b = sp.cos(x[:, 1])*sp.cos(y1[i, 1])*sp.cos(x[:, 0] - y1[i, 0])
        c = sp.arccos(a + b)
        #cc = sp.array([y[i, 2], ] * len(x))
        s1 = 1 - (R * c.real/y[i, 2])**2

        s1[s1 < 0] = 0
        s1 = sp.sparse.csr_matrix(s1)
        s1 = s1.multiply(s1)
        row_ind.extend(s1.indices)
        col_ind.extend([i]*len(s1.indices))
        data.extend(s1.data)

    d = sp.sparse.csc_matrix((data, (row_ind, col_ind)), shape=(x.shape[0], y.shape[0]))

    return d
