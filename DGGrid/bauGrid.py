# -*- coding: utf-8 -*-
"""
Created on Mon Apr 27 15:42:56 2015

@author: marchett
"""

import scipy as sp
import collections
from .Grid import *


# buffer grid needs to be fixed to include wrapping
# around the globe, e.g. going from 90 to -90

# create a square or hexagonal BAU grid
class bauGrid(object):
  def __init__(self, win, res, type, degr_km = 100):
    # Initialize Instance Variables
    self.type          = type # { square, hex }
    self.window        = win
    if type == 'square':
      self.resolution  = res
      self.level       = None
    else:
      self.resolution  = (7680, 3840, 1920, 960, 480, 240, 120, 60, 
                         30, 15, 7, 4, 2, 1)[res]
      self.level       = res
    
    self.toDegree      = degr_km
    self.locations     = None
    self.longitudes    = None
    self.latitudes     = None
    self.indicator     = None
    self.interface     = None
    self.ids           = None 
    self.iw            = None


  # generate a new grid with or without buffer  
  def newGrid(self, buffer = 0, basis = None):
    if self.resolution is not None:
      res = self.resolution
    else:
      print("ERROR: cannot create new grid, resolution is undefined.")
      return
    
    if self.type == 'square':
      lonwin = self.window[3] - self.window[2]
      latwin = self.window[1] - self.window[0]
      grid = sp.ceil(((latwin * self.toDegree) / res, 
                      (lonwin * self.toDegree) / res))
    
      x, size = sp.linspace(self.window[2], self.window[3], grid[0], retstep=True)
      y, size = sp.linspace(self.window[0], self.window[1], grid[1], retstep=True)    
      lonsnew, latsnew = sp.meshgrid(x, y)
      self.locations = sp.vstack((sp.hstack(lonsnew), sp.hstack(latsnew))).T
    
      self.longitudes = lonsnew[0, :]
      self.latitudes = sp.hstack([x[0] for x in latsnew])
      self.indicator = sp.repeat(True, len(self.locations))
      
      if buffer > 0:
        gridB = BAUgrid(self.window, res = res * 5, type = self.type)  
        gridB.newSquareBuffer(buffer = buffer, maxaper = sp.amax(basis.aperture))
        self.mergeGrid(gridB)
        self.indicator = sp.r_[self.indicator, sp.repeat(False, len(self.locations))]
    
    if self.type == 'hex': 
      if self.window != [-90, 90, -180, 180]:
        hg = Grid(self.level)
        hg.newFullGrid(type = 'empty')
        iw = self.resolution/2./self.toDegree
        extWin = sp.sum([self.window, [- iw, iw, - iw, iw]], axis = 0)
        locs = hg.cellFloodFillRegion(extWin)
        ID = sp.ravel(locs.keys())
        locs = sp.vstack([x[1] for x in locs.values()])
        isort = sp.argsort(ID)
        
        self.locations = [locs[isort, :]]
        self.indicator = sp.repeat(True, len(locs))
        self.interface = [hg]
        self.ids = [ID[isort]]
      else:
        hg = Grid(self.level)
        hg.newFullGrid(type = 'dgg')
        
        self.locations = [hg.idxCellCenterMat]
        self.indicator = sp.repeat(True, len(self.locations[0]))
        self.interface = [hg]
        qcids = [hg.q2diToCellIdx(q2di) 
                   for q2di 
                   in hg.lonLatMatToQ2diMat(sp.hstack(self.locations))]
        self.ids = [sp.ravel(qcids)]           
      
      if buffer > 0:
        self.newHexBuffer(buffer = buffer, maxaper = sp.amax(basis.aperture))
     
      self.locations = sp.vstack(self.locations)

      
  # hexagonal buffer
  def newHexBuffer(self, buffer, maxaper):
    if self.resolution is not None:
      res = self.resolution
    else:
      print("ERROR: cannot create new buffer grid, resolution is undefined.")
      return      
    
    lonwin = self.window[3] - self.window[2]
    latwin = self.window[1] - self.window[0]
      
    diag = sp.sqrt((lonwin)**2 + (latwin)**2)
    iw = ((diag * self.toDegree) / maxaper) * buffer
    if self.resolution > iw * self.toDegree:
      resbuf = (7680, 3840, 1920, 960, 480, 240, 120, 60, 
                         30, 15, 7, 4, 2, 1)[self.level - 3]  
      iw = resbuf / float(self.toDegree)
    
    expand = sp.sum([self.window, [- iw, iw, - iw, iw]], axis = 0)
    
    lat1 = sp.repeat([[expand[0]], [self.window[1]], [self.window[0]]], 3)
    lat2 = sp.repeat([[self.window[0]], [expand[1]], [self.window[1]]], 3)
    lon1 = sp.tile([expand[2], self.window[2], self.window[3]], 3)
    lon2 = sp.tile([self.window[2], self.window[3], expand[3]], 3)
    bzone = sp.c_[lat1, lat2, lon1, lon2]
    bzone = sp.delete(bzone, 7, axis = 0) 
    
    for i in range(len(bzone)):
      bg = Grid(self.level - 1)
      bg.newFullGrid(type = 'empty')
      locs = bg.cellFloodFillRegion(bzone[i])
      ID = sp.ravel(locs.keys())
      locs = sp.vstack([x[1] for x in locs.values()])
      isort = sp.argsort(ID)
      self.locations.append(locs[isort, :])
      self.ids.append(ID[isort])
      self.interface.append(bg)
    
    self.locations = sp.vstack(self.locations)
    self.window = expand
    self.iw = iw



  # matching and aggregating data on a hex grid
  def hexCounts(self, newlocs, weights = None):
      if len(newlocs) == 0:         
          emptylist = [sp.zeros(len(sp.hstack(self.ids)))]
          binvals = emptylist * len(weights)
          indices = sp.array([]).reshape((0, 2))
      else:    
          weights = sp.atleast_1d(weights)
          ngrids = len(self.interface)
          binvals = [sp.zeros(0)] * len(weights)
          indicesRow = []
          indicesCol = []
          for n in range(ngrids):
              qcids = [self.interface[n].q2diToCellIdx(q2di) 
                       for q2di 
                       in self.interface[n].lonLatMatToQ2diMat(newlocs)]
              qcids = sp.ravel(qcids)
              matchmask = sp.in1d(qcids, self.ids[n])
              test = qcids[matchmask]
              a1 = sp.unique(test)
              a2 = sp.searchsorted(self.ids[n], sp.unique(test))
              [sp.place(test, test == x, y) for x, y in zip(a1, a2)]
              for j, w in enumerate(weights):
                  if w is not None:
                      w = w[matchmask]
                  binvals[j] = sp.r_[binvals[j], 
                                     sp.bincount(test, weights=w, 
                                                 minlength=len(self.ids[n]))]
          
              indicesRow.extend(sp.arange(len(newlocs))[matchmask])
              indicesCol.extend(test)
          
          indices = sp.c_[sp.ravel(indicesRow), sp.ravel(indicesCol)]
          
      return binvals, indices
          
           
  # square buffer  
  def newSquareBuffer(self, buffer, maxaper):
    if self.resolution is not None:
      res = self.resolution
    else:
      print("ERROR: cannot create new buffer grid, resolution is undefined.")
      return
    
    lonwin = self.window[3] - self.window[2]
    latwin = self.window[1] - self.window[0]
      
    diag = sp.sqrt((lonwin)**2 + (latwin)**2)
    iw = ((diag * self.toDegree) / maxaper) * buffer
    if self.resolution > iw * self.toDegree:
      iw = self.resolution / float(self.toDegree)
    expand = sp.sum([self.window, [- iw, iw, - iw, iw]], axis = 0)
      
    lonwin = expand[3] - expand[2]
    latwin = expand[1] - expand[0]
    grid = sp.ceil(((latwin * self.toDegree) / res, 
                    (lonwin * self.toDegree) / res))
    
    x, size = sp.linspace(expand[2], expand[3], grid[0], retstep=True)
    y, size = sp.linspace(expand[0], expand[1], grid[1], retstep=True)    
    lonsnew, latsnew = sp.meshgrid(x, y)
    self.locations = sp.vstack((sp.hstack(lonsnew), sp.hstack(latsnew))).T
    self.longitudes = lonsnew[0, :]
    self.latitudes = sp.hstack([x[0] for x in latsnew])
    
    
    ilat = sp.logical_or(self.locations[:, 1] < self.window[0], 
                         self.locations[:, 1] > self.window[1]).nonzero()[0]
    ilon = sp.logical_or(self.locations[:, 0] < self.window[2], 
                         self.locations[:, 0] > self.window[3]).nonzero()[0]
    ind = sp.sort(sp.unique(sp.r_[ilat, ilon]))
    self.locations = self.locations[ind]

    
    ilon = sp.logical_or(self.longitudes < self.window[2], 
                         self.longitudes > self.window[3]).nonzero()[0]
    self.longitudes = self.longitudes[ilon]                     
    ilat = sp.logical_or(self.latitudes < self.window[0], 
                         self.latitudes > self.window[1]).nonzero()[0]
    self.latitudes = self.latitudes[ilat]
    
    self.window = expand
    self.iw = iw
    
     
  # matching new locations to a square or hex grid   
  def matchGrid(self, newlocs, weights = None):
    weights = sp.atleast_1d(weights)
    p = len(weights)
    
    if self.type == 'square':
      x = len(self.longitudes)
      y = len(self.latitudes)
      index = sp.arange(x * y)
    
      indgrid = sp.array(index).reshape((x, y), order='F')
      xyi1 = sp.searchsorted(self.longitudes, newlocs[:, 0])
      xyi2 = sp.searchsorted(self.latitudes, newlocs[:, 1])
    
      indicesCol = indgrid[xyi1, xyi2]
      indicesRow = sp.arange(len(newlocs))
      indices = sp.c_[sp.ravel(indicesRow), sp.ravel(indicesCol)]
      binvals = [None] * p
      for j, w in enumerate(weights):
        binvals[j] = sp.bincount(indicesCol, weights=w, minlength=len(index))
    
    if self.type == 'hex':  
      binvals, indices = self.hexCounts(newlocs, weights)
         
    vals = [''.join(z) for z in sp.c_[sp.repeat('V', p), range(1,p+1)]]
    vals.append('indices')
    binvals.append(indices)
    point = collections.namedtuple('Point', vals)
    output = point._make(binvals)    
    
    return output


  # merge two square grids
  def mergeGrid(self, newGrid):
    if self.resolution != newGrid.resolution:  
      self.resolution = (self.resolution, newGrid.resolution)    
    self.window = newGrid.window
    self.longitudes = sp.sort(sp.r_[self.longitudes, newGrid.longitudes])
    self.latitudes = sp.sort(sp.r_[self.latitudes, newGrid.latitudes])
    lonsnew, latsnew = sp.meshgrid(self.longitudes, self.latitudes)
    self.locations = sp.vstack((sp.hstack(lonsnew), sp.hstack(latsnew))).T
  
   
    
    
    
    
