# Copyright [2014]. California Institute of Technology.  ALL RIGHTS RESERVED.
# U.S. Government sponsorship acknowledged. Any commercial use must be
# negotiated with the Office of Technology Transfer at the California
# Institute of Technology.

# This software is subject to U. S. export control laws and regulations
# (22 C.F.R. 120-130 and 15 C.F.R. 730-774). To the extent that the
# software is subject to U.S. export control laws and regulations, the
# recipient has the responsibility to obtain export licenses or other
# export authority as may be required before exporting such information
# to foreign countries or providing access to foreign nationals.

import os
from .dgglibc import *

# Global Variables
if 'OSTYPE' in os.environ:
  OSTYPE     = os.environ['OSTYPE']
else:
  OSTYPE     ='linux'
if 'HOME' in os.environ:
  HOME       = os.environ['HOME']
else:
  HOME       = '.'

### DEPRICATED ***
# CACHE_DIR  = '/data/home/stough/proj/mvdf-sim/dgg/cache'
# DGGRID_CMD = (HOME + '/proj/mvdf-sim/tps/dggrid-%s' % OSTYPE)

# Height at which KML polygons are generated
KML_HEIGHT = 100000

# DGG Stats in lists indexed by resolution
cellAreaKm2 = [ 42500000, 12751646.91, 3187911.727, 796977.932, 199244.483,
                49811.121, 12452.78, 3113.195, 778.299, 194.575, 48.644,
                12.161, 3.04, 0.76, 0.19 ]
cellDistKm  = [ 7674.46, 3837.23, 1918.615, 959.307, 479.654, 239.827,
                119.913, 59.957, 29.978, 14.989, 7.495, 3.747, 1.874, 0.937,
                0.468 ]
cellCLSKm   = [ -999, 4046.361, 2016.794, 1007.607, 503.705, 251.84, 125.919,
                62.959, 31.48, 15.74, 7.87, 3.935, 1.967, 0.984, 0.492 ]

# Initialize the dgglibc state object and store it.
# Number of DGG resolutions in pyramid MAX_RES < NUM_RES
NUM_RES      = 14
DENSIFY      = 0
dggLibCState = new_dggs4h(NUM_RES)

# Contental US bounding region
# Where region is (minlat, maxlat, minlon, maxlon)
CONUS_REGION = (24.396308, 49.384358, -124.848974, -66.885444)
SO_AM_REGION = (-45.0, 0.0, -75.0, -45.0)
SO_CAL_REGION = (33, 34, -118.5, -116)
    
################################################################################
# TEMPLATES for kml generation
################################################################################

KML_HEADER = '''<?xml version="1.0" encoding="UTF-8"?>
<kml xmlns="http://www.opengis.net/kml/2.2" xmlns:gx="http://www.google.com/kml/ext/2.2" xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom">
  <Document>
    <name>KmlFile</name>
    <StyleMap id="msn_ylw-pushpin">
      <Pair>
        <key>normal</key>
        <styleUrl>#sn_ylw-pushpin</styleUrl>
      </Pair>
      <Pair>
        <key>highlight</key>
        <styleUrl>#sh_ylw-pushpin</styleUrl>
      </Pair>
    </StyleMap>
    <Style id="sh_ylw-pushpin">
      <IconStyle>
        <scale>1.3</scale>
        <Icon>
          <href>http://maps.google.com/mapfiles/kml/pushpin/ylw-pushpin.png</href>
        </Icon>
        <hotSpot x="20" y="2" xunits="pixels" yunits="pixels"/>
      </IconStyle>
      <LineStyle>
        <color>ff1410ff</color>
        <width>2</width>
      </LineStyle>
      <PolyStyle>
        <fill>0</fill>
      </PolyStyle>
    </Style>
    <Style id="sn_ylw-pushpin">
      <IconStyle>
        <scale>1.1</scale>
        <Icon>
          <href>http://maps.google.com/mapfiles/kml/pushpin/ylw-pushpin.png</href>
        </Icon>
        <hotSpot x="20" y="2" xunits="pixels" yunits="pixels"/>
      </IconStyle>
      <LineStyle>
        <color>ff1410ff</color>
        <width>2</width>
      </LineStyle>
      <PolyStyle>
        <fill>0</fill>
      </PolyStyle>
    </Style>
    <StyleMap id="msn_red-circle">
      <Pair>
        <key>normal</key>
        <styleUrl>#sn_red-circle</styleUrl>
      </Pair>
      <Pair>
        <key>highlight</key>
        <styleUrl>#sh_red-circle</styleUrl>
      </Pair>
    </StyleMap>
    <Style id="sh_red-circle">
      <IconStyle>
        <scale>1.3</scale>
        <Icon>
          <href>http://maps.google.com/mapfiles/kml/paddle/red-circle.png</href>
        </Icon>
        <hotSpot x="32" y="1" xunits="pixels" yunits="pixels"/>
      </IconStyle>
      <ListStyle>
        <ItemIcon>
          <href>http://maps.google.com/mapfiles/kml/paddle/red-circle-lv.png</href>
        </ItemIcon>
      </ListStyle>
    </Style>
    <Style id="sn_red-circle">
      <IconStyle>
        <scale>1.1</scale>
        <Icon>
          <href>http://maps.google.com/mapfiles/kml/paddle/red-circle.png</href>
        </Icon>
        <hotSpot x="32" y="1" xunits="pixels" yunits="pixels"/>
      </IconStyle>
      <ListStyle>
        <ItemIcon>
          <href>http://maps.google.com/mapfiles/kml/paddle/red-circle-lv.png</href>
        </ItemIcon>
      </ListStyle>
    </Style>'''

# coordinates are in comma separated triplets of lat lon height:
#
#    name = <string>
#    coordlist = "lat1, lon1, h1, lat2, lon2, h2, ... latn, lonn, hn"
#    center = "lat, lon"
#    color = "hhhhhhhh" in ABGR
        # <Placemark>
	# 	<description>%(name)s</description>
        #         <styleUrl>#msn_red-circle</styleUrl>
	# 	<Point>
	# 		<altitudeMode>clampToGround</altitudeMode>
	# 		<gx:altitudeMode>clampToSeaFloor</gx:altitudeMode>
	# 		<coordinates>%(center)s,0</coordinates>
	# 	</Point>
	# </Placemark>
KML_POLY = '''
    <Folder>
        <name>%(name)s</name>
        <Placemark>
		<name>%(name)s</name>
                <Style> 
                     <LineStyle>
                       <color>ff1410ff</color>
                       <width>2</width>
                     </LineStyle>
                     <PolyStyle>
                       <fill>1</fill>
                       <color>%(cellColor)s</color>
                       <outline>0</outline> 
                     </PolyStyle> 
                </Style>
		<Polygon>
			<altitudeMode>relativeToGround</altitudeMode>
			<outerBoundaryIs>
				<LinearRing>
					<coordinates>%(coordlist)s</coordinates>
				</LinearRing>
			</outerBoundaryIs>
		</Polygon>
                %(kmlRegion)s
	</Placemark>
    </Folder>'''
KML_POLY_OUTL = '''
    <Folder>
        <name>%(name)s</name>
        <Placemark>
		<name>%(name)s</name>
                <Style> 
                     <LineStyle>
                       <color>ff1410ff</color>
                       <width>2</width>
                     </LineStyle>
                     <PolyStyle>
                       <fill>1</fill>
                       <color>%(cellColor)s</color>
                       <outline>1</outline> 
                     </PolyStyle> 
                </Style>
		<Polygon>
			<altitudeMode>relativeToGround</altitudeMode>
			<outerBoundaryIs>
				<LinearRing>
					<coordinates>%(coordlist)s</coordinates>
				</LinearRing>
			</outerBoundaryIs>
		</Polygon>
                %(kmlRegion)s
	</Placemark>
    </Folder>'''

KML_FOLDER_OPEN = '''
    <Folder>
        <name>%(name)s</name>'''

KML_FOLDER_CLOSE = '''
    </Folder>'''

KML_NETWORKLINK = '''
    <NetworkLink>
      <name>%(name)s</name>
      %(kmlRegion)s
      <Link>
        <href>%(relPath)s</href>
        <viewrefreshmode>onRegion</viewrefreshmode>
      </Link>
    </NetworkLink>'''

KML_REGION = '''
      <Region>
        <LatLonAltBox>
          <west> %(minLon)f</west>
          <south>%(minLat)f</south>
          <east> %(maxLon)f</east>
          <north>%(maxLat)f</north>
          <minAltitude>100000</minAltitude>
          <maxAltitude>100000</maxAltitude>
          <altitudeMode>relativeToGround</altitudeMode>
        </LatLonAltBox>
        <Lod>
          <minLodPixels>%(minLodPix)d</minLodPixels>
          <maxLodPixels>%(maxLodPix)d</maxLodPixels>
          <minFadeExtent>%(minFade)d</minFadeExtent>
          <maxFadeExtent>%(maxFade)d</maxFadeExtent>
        </Lod>
      </Region>'''

KML_FOOTER = '''</Document>
</kml>
'''
