# Copyright [2007]. California Institute of Technology.  ALL RIGHTS RESERVED.
# U.S. Government sponsorship acknowledged. Any commercial use must be
# negotiated with the Office of Technology Transfer at the California
# Institute of Technology.

# This software is subject to U. S. export control laws and regulations
# (22 C.F.R. 120-130 and 15 C.F.R. 730-774). To the extent that the
# software is subject to U.S. export control laws and regulations, the
# recipient has the responsibility to obtain export licenses or other
# export authority as may be required before exporting such information
# to foreign countries or providing access to foreign nationals.

# Import needed scipy/pylab/etc...
import numpy as np

# ctypes interfacing
import ctypes
from ctypes import cdll
from numpy.ctypeslib import ndpointer


PATH_TO_LIBRARY = '/DGGrid/src/apps/dgglibc'

dgglib = cdll.LoadLibrary(PATH_TO_LIBRARY + '/dgglibc.so')
# dgglib = cdll.LoadLibrary(PATH_TO_LIBRARY + '/dgglibc.dylib')

## NEW_DGGS4H
__new_dggs4h = dgglib.new_dggs4h
__new_dggs4h.restype = ctypes.c_void_p
__new_dggs4h.argtypes = [ ctypes.c_int ]
def new_dggs4h(numResolutions):
  return __new_dggs4h(numResolutions)

## DELETE_DGGS4H
__delete_dggs4h = dgglib.delete_dggs4h
__delete_dggs4h.restype = ctypes.c_int
__delete_dggs4h.argtypes = [ ctypes.c_void_p ]
def delete_dggs4h(dggState):
  return __delete_dggs4h(dggState)

## Q2DI_TO_CENTERS
# centers are in lon/lat format
__q2di_to_centers = dgglib.q2di_to_centers
__q2di_to_centers.restype  = ctypes.c_int
__q2di_to_centers.argtypes = \
    [ ctypes.c_void_p, ctypes.c_int,
      ndpointer(ctypes.c_longlong, flags="C_CONTIGUOUS"), ctypes.c_int,
      ndpointer(ctypes.c_double, flags="C_CONTIGUOUS") ]
def q2di_to_centers(dggState, resolution, q2diArray):
  if len(q2diArray.shape) == 1:
    nQ2di = 1
  else:
    nQ2di = q2diArray.shape[0]

  q2dis      = np.ascontiguousarray(q2diArray, dtype=np.int64)
  centers    = np.empty((nQ2di, 2), dtype=np.double)
  centers[:] = np.NAN

  __q2di_to_centers(dggState, resolution,
                    q2dis, nQ2di, centers)

  return centers

## Q2DI_TO_GEO_COORDS
# centers and polygons are in lon/lat format
__q2di_to_geo_coords = dgglib.q2di_to_geo_coords
__q2di_to_geo_coords.restype  = ctypes.c_int
__q2di_to_geo_coords.argtypes = \
    [ ctypes.c_void_p, ctypes.c_int, ctypes.c_int,
      ndpointer(ctypes.c_longlong, flags="C_CONTIGUOUS"), ctypes.c_int,
      ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),
      ndpointer(ctypes.c_double, flags="C_CONTIGUOUS") ]
def q2di_to_geo_coords(dggState, resolution, densify, q2diArray):
  if len(q2diArray.shape) == 1:
    nQ2di = 1
  else:
    nQ2di = q2diArray.shape[0]

  nPtsInPoly = (densify + 1) * 6

  q2dis      = np.ascontiguousarray(q2diArray, dtype=np.int64)
  centers    = np.empty((nQ2di, 2), dtype=np.double)
  centers[:] = np.NAN
  polys      = np.empty((nQ2di, nPtsInPoly, 2), dtype=np.double)
  polys[:]   = np.NAN

  __q2di_to_geo_coords(dggState, resolution, densify,
                       q2dis, nQ2di, centers, polys)

  return (centers, polys)

## LONLATMAT_TO_Q2DI
__lonLatMat_to_q2di = dgglib.lonLatMat_to_q2di
__lonLatMat_to_q2di.restype = ctypes.c_int
__lonLatMat_to_q2di.argtypes = \
    [ ctypes.c_void_p, ctypes.c_int,
      ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),
      ctypes.c_int, ctypes.c_int,
      ndpointer(ctypes.c_longlong, flags="C_CONTIGUOUS") ]
def lonLatMat_to_q2di(dggState, resolution, lonLatMat):

  llm = np.ascontiguousarray(lonLatMat[:,0:2], dtype=np.double)
  
  rdim = llm.shape[0]
  cdim = llm.shape[1]

  q2dis = np.zeros((rdim,3), dtype=np.int64)

  __lonLatMat_to_q2di(dggState, resolution, llm, rdim, cdim, q2dis)

  return q2dis
                 

## TEST
# import dgglibc
# import numpy as np
# s = dgglibc.new_dggs4h(14)
# q2di = np.array([1,2,3])
# densify = 2
# res = 8

# (centers,polys) = dgglibc.q2di_to_geo_coords(s,res,densify,q2di); centers; ploys

# lonlat=polys[0,:,(1,0)].transpose()
# q2dis = dgglibc.lonLatMat_to_q2di(s,res, lonlat); q2dis
