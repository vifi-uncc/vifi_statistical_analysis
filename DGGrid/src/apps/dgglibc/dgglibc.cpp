////////////////////////////////////////////////////////////////////////////////
//
// dgglibc.cpp: 
//
// Tim Stough, 7/21/2014
//
////////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <cmath>

using namespace std;

#include "DgIDGGS4H.h"

int test_CPP (void);

extern "C" {
  typedef struct {
    int         numRes;
    DgRFNetwork *net;
    DgGeoSphRF  *geoRF;
    DgIDGGS4H   *idggs;
  } DGGStateStruct;

  DGGStateStruct *new_dggs4h(int numRes) {
    DGGStateStruct *ret = new DGGStateStruct;
    ret->numRes = numRes;
    
    // create the reference frame (RF) conversion network
    ret->net = new DgRFNetwork();

    // create the geodetic reference frame
    ret->geoRF = new DgGeoSphRF(*(ret->net), "GS0");

    // create the ISEA4H grid system with resolutions 0-9; requires a
    // fixed icosahedron vertex and edge azimuth
    DgGeoCoord vert0(11.25L, 58.28252559L, false); // args: lon, lat, isRadians
    long double azimuth = 0.0L;

    // last argument is number of resolutions
    ret->idggs = new DgIDGGS4H(*(ret->net), *(ret->geoRF), vert0, azimuth,
                               numRes);

    return ret;
  }

  int delete_dggs4h(DGGStateStruct *dggs) {
    delete dggs->idggs;
    delete dggs->geoRF;
    delete dggs->net;
    return 0;
  }

  int q2di_to_centers(// Input parameters
                      DGGStateStruct *dggs, int res,
                      long long int *pllQ2di, int nQ2di,
                      // Output parameters
                      double *pdCenters) {
    const DgIDGG& dgg = dggs->idggs->idgg(res);

    // loop over all q2di coords
    for (int i = 0; i < nQ2di; i++) {
      // The q2di array may not be full.  The end is marked by -999.
      if (pllQ2di[3 * i] == -999) break;
      
      // computer current pointers into the input and outputs
      long long int *pllCurQ2di     = &(pllQ2di[3 * i]);
      double        *pdCurCenter    = &(pdCenters[2 * i]);

      // extract the current q2di coord
      long long int q    = pllCurQ2di[0];
      long long int iIdx = pllCurQ2di[1];
      long long int jIdx = pllCurQ2di[2];

      // map the q2di to a cell and get the center
      DgQ2DICoord curQ2di(q, DgIVec2D(iIdx, jIdx));
      DgLocation *curCell = dgg.makeLocation(curQ2di);
      dggs->geoRF->convert(curCell);
      const DgGeoCoord& centCoord = *(dggs->geoRF->getAddress(*curCell));
      // extract as lon/lat for center
      pdCurCenter[0] = centCoord.lonDegs();
      pdCurCenter[1] = centCoord.latDegs();
    }
      
    return 0;
  }
    
  
  // Centers and Polygons, and all geo-coord tuples are in lon/lat order
  int q2di_to_geo_coords(// Input parameters
                         DGGStateStruct *dggs, int res, int densify,
                         long long int *pllQ2di, int nQ2di,
                         // Output parameters
                         double *pdCenters, double *pdPolyCoords) {

    // cout << "resolution: " << res << endl;
    // cout << "denisfy: " << densify << endl;
    // cout << "q[0]: " << pllQ2di[0] << endl;
    // cout << "nQ2di: " << nQ2di << endl;
    
    const DgIDGG& dgg = dggs->idggs->idgg(res);

    // a polygon has 6 verts plus [densify] per side
    const int ptsPerPoly = (densify + 1) * 6;
    const int ptsPerPent = (densify + 1) * 5;

    // loop over all q2di coords
    for (int i = 0; i < nQ2di; i++) {
      // The q2di array may not be full.  The end is marked by -999.
      if (pllQ2di[3 * i] == -999) break;
      
      // computer current pointers into the input and outputs
      long long int *pllCurQ2di     = &(pllQ2di[3 * i]);
      double        *pdCurCenter    = &(pdCenters[2 * i]);
      double        *pdCurPolyStart = &(pdPolyCoords[ptsPerPoly * 2 * i]);

      // extract the current q2di coord
      long long int q    = pllCurQ2di[0];
      long long int iIdx = pllCurQ2di[1];
      long long int jIdx = pllCurQ2di[2];

      // map the q2di to a cell and get the center
      DgQ2DICoord curQ2di(q, DgIVec2D(iIdx, jIdx));
      DgLocation *curCell = dgg.makeLocation(curQ2di);
      dggs->geoRF->convert(curCell);
      const DgGeoCoord& centCoord = *(dggs->geoRF->getAddress(*curCell));
      // extract as lon/lat for center
      pdCurCenter[0] = centCoord.lonDegs();
      pdCurCenter[1] = centCoord.latDegs();

      // get the verticies of the cell polygon
      DgPolygon verts;
      dgg.convert(curCell);
      // densify specifies how many points per side in addition the the verts
      dgg.setVertices(*curCell, verts, densify);
      // loop over all the points in the poly
      for (int j = 0; j < ptsPerPoly; j++) {
        // skip the mising verts
        if ((iIdx == 0) & (jIdx == 0) & (j == ptsPerPent)) break;
        
        // compute pointer for current vert
        double *pdCurVert = &(pdCurPolyStart[2 * j]);

        // extract the lon/lat coords for each vert
        const DgGeoCoord& curVert = *(dggs->geoRF->getAddress(verts[j]));
        pdCurVert[0] = curVert.lonDegs();
        pdCurVert[1] = curVert.latDegs();
      }
    }
      
    return 0;
  }

  int lonLatMat_to_q2di(// Input parameters
                        DGGStateStruct *dggs, int res,
                        double *pdLonLatMat, int rdim, int cdim,
                        // Output parameters
                        long long int *pllQ2di) {
    // grab the dgg at the res we're working at
    const DgIDGG& dgg = dggs->idggs->idgg(res);
    
    // Loop over all rows on the lonLatMat
    for (int i = 0; i < rdim; i++) {
      // compute pointers for current inputs and output
      double *pdCurLonLatRow = &(pdLonLatMat[cdim * i]);
      long long int *pllCurQ2di = &(pllQ2di[3 * i]);

      // extract lat/lon
      double lat = pdCurLonLatRow[1];
      double lon = pdCurLonLatRow[0];

      // convert lat/lon to dgg cell
      DgGeoCoord geoAddress(lon, lat, false);
      DgLocation *curCell = dggs->geoRF->makeLocation(geoAddress);
      // cout << "cur cell " << *curCell << endl;
      dgg.convert(curCell);
      // cout << "lies in cell " << *curCell << endl;

      
      // get q2di address to dgg cell
      const DgQ2DICoord& gridQ2DICoord = *dgg.getAddress(*curCell);
      pllCurQ2di[0] = gridQ2DICoord.quadNum();
      pllCurQ2di[1] = gridQ2DICoord.coord().i();
      pllCurQ2di[2] = gridQ2DICoord.coord().j();

    }
    return 0;
  }


  int q2diPaddingMapFwd(// Input parameters
                        long long int q, long long int i, long long int j,
                        int res,
                        // Output parameters
                        long long int *pllQ2di) {
    long long int qp, ip, jp;
    int n = res;
    
    if (q < 6) {
      qp = (q-1 + 1) % 5 + 1;
      ip = j - pow(2.0, n);
      jp = j - i;
    } else {
      ; // FIXME - Continue the C implementation if needed
      qp = ip = jp = -999;
    }

    pllQ2di[0] = qp;
    pllQ2di[1] = ip;
    pllQ2di[2] = jp;

    return 0;
  }
    
  int test(void) { return test_CPP(); }

}

////////////////////////////////////////////////////////////////////////////////
int test_CPP (void)
{
   ///// create the DGG /////

   // create the reference frame (RF) conversion network
   DgRFNetwork net0;

   // create the geodetic reference frame
   DgGeoSphRF geoRF(net0, "GS0");

   // create the ISEA4H grid system with resolutions 0-9; requires a
   // fixed icosahedron vertex and edge azimuth
   DgGeoCoord vert0(11.25L, 58.28252559L, false); // args: lon, lat, isRadians
   long double azimuth = 0.0L;
   const DgIDGGS4H idggs(net0, geoRF, vert0, azimuth, 14); // last argument is
                                                      // number of resolutions

   // get the resolution specific dgg from the dggs
   const DgIDGG& dgg = idggs.idgg(1);

   //////// now use the DGG /////////

   ///// given a point in lon/lat, get the cell it lies within /////

   // first create a DgLocation in geoRF coordinates
   DgGeoCoord geoAddress(0, 54.0, false);
   DgLocation* thePt = geoRF.makeLocation(geoAddress);
   cout << "the point " << *thePt << endl;

   // converting the point location to the dgg RF determines which cell it's in
   dgg.convert(thePt);
   cout << "* lies in cell " << *thePt << endl;

   // get at the dgg RF coords
   const DgQ2DICoord& gridQ2DICoord = *dgg.getAddress(*thePt);
   int quad = gridQ2DICoord.quadNum();
   long long int i = gridQ2DICoord.coord().i();
   long long int j = gridQ2DICoord.coord().j();

   cout << "quad: " << quad << " i(" << i << "), j(" << j << ")" << endl;

   // we can get the cell's vertices, which are defined in geoRF
   DgPolygon verts;
   int ptsPerEdgeDensify = 2;
   dgg.setVertices(*thePt, verts, ptsPerEdgeDensify);
   cout << "* with cell boundary:\n" << verts << endl;

   // we can get the cell's center point by converting the cell back to geoRF
   geoRF.convert(thePt);
   cout << "* and cell center point:" << *thePt << endl;

   // we can extract the coordinates into primitive data types
   const DgGeoCoord& centCoord = *geoRF.getAddress(*thePt);
   double latRads = centCoord.lat();
   double lonRads = centCoord.lon();
   cout << "* center point lon,lat in radians: " 
        << lonRads << ", " << latRads << endl;

   const DgGeoCoord& firstVert = *geoRF.getAddress(verts[0]);
   double latDegs = firstVert.latDegs();
   double lonDegs = firstVert.lonDegs();
   cout << "* first boundary vertex lon,lat in degrees: " 
        << lonDegs << ", " << latDegs << endl;

   return 0;

} // main 

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
