FROM continuumio/miniconda3

RUN apt-get update --fix-missing && \
    apt-get install -y libgl1-mesa-glx build-essential gdal-bin libgdal-dev
	#mesa-libGL libXcomposite libXcursor libXi libXtst libXScrnSaver libXrandr alsa-lib mesa-libEGL

# Install required python modules, and clone the DGGRID library
RUN conda install -y python pandas matplotlib numpy scipy scikit-learn && \
conda install -y -c conda-forge pyyaml scikit-sparse gdal && \
conda clean -y --all

# Copy required libraries
COPY common_fns/ ./
COPY DGGrid /DGGrid

# Download DGGRID, copy MakeIncludes to correct location
RUN cd DGGrid/src && \
make && \
cd && \
conda update gdal

# Append DGGrid to PATH
CMD export PATH=/DGGrid/src/apps/dggrid:$PATH;bash
