import numpy as np
import pandas as pd

from matplotlib import pyplot as plt
from scipy.spatial.distance import pdist, squareform

#### Functions necessary to compute gradients on servers 1 and 2 ####
def exp_cov_log(x, phi):
    return(np.exp(-x/np.exp(phi)))
    
def exp_deriv_log(x, phi, Sigma):
    return(x * np.exp(-phi) * Sigma)    
    
def nll_gradient_fun(theta, ys, ds, tau, cov_fun, deriv_fun):
    Sigma = cov_fun(ds, theta) + tau * np.identity(len(ds))
    dSigma = deriv_fun(ds, theta, Sigma)
    C = np.linalg.cholesky(Sigma)
    Ci = np.linalg.inv(C)
    SigInv = Ci.T @ Ci
    grads = 0.5*sum(np.diag(SigInv@dSigma)) - 0.5*np.transpose(ys)@SigInv@dSigma@SigInv@ys
    nll = sum(np.log(np.diag(C))) + 0.5*sum(np.linalg.solve(C,ys)**2)
    return([grads, nll])

