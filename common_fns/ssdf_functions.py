import numpy as np
import scipy as sp
import pandas as pd
from sksparse.cholmod import cholesky
from numpy.linalg import inv
import random, sys

sys.path.append('/')
from DGGrid import *
from ssdf_functions import *

#### Functions necessary to compute gradients on servers 1 and 2 ####
def comp_sep_components(sig2_delta, sig2_eps_vec, S, E, z):
    V = sp.sparse.csc_matrix(sig2_delta*E + sp.sparse.diags(sig2_eps_vec, 0) )
    factor = cholesky(V)
    Vi = factor.inv()
    
    ViS = Vi@S
    Viz = Vi@z

    a = np.transpose(z)@Viz + factor.logdet()
    gamma = S.T@Viz
    R = S.T@ViS
    
    alpha = Viz.T@(E@Viz) - sp.sum(E.multiply(Vi))
    kappa = (Viz.T@E)@ViS 
    Q = ViS.T@E@ViS
        
    return({"R" : sp.sparse.tril(R), "gamma" : gamma, "alpha" : alpha, "kappa" : kappa.T, "Q" : sp.sparse.tril(Q), "a" : a})

def get_pred(vz, Kz, S_ts, H_ts, z_ts, sig2_delta):

  mu_est = S_ts @ vz
  S_ts2 = S_ts.toarray()
  var_est = np.add(np.sum(np.multiply(S_ts2@Kz,S_ts2), axis=1) , np.sum(H_ts.multiply(H_ts), axis=1)*sig2_delta)
  
  ss = np.sum(np.power(mu_est - z_ts,2))
  n2 = len(z_ts)
  return({"mu_est" : mu_est, "var_est" : var_est, "n2" : n2, "ss" : ss})
  
def get_pred_only(vz, Kz, S_ts, z_ts):

  mu_est = S_ts @ vz  
  ss = np.sum(np.power(mu_est - z_ts,2))
  n2 = len(z_ts)
  return({"mu_est" : mu_est, "n2" : n2, "ss" : ss})
    
def em_steps(sig2_delta, K0i, R, gamma, alpha, kappa, Q, n):
    gamma = np.array(gamma)
    kappa = np.array(kappa)
    
    Kzi = K0i + R
    Kz = inv(Kzi)
    #vz = np.array(Kz@gamma)
    vz = Kz@gamma
#    sd_new = sig2_delta + 1./n*(sig2_delta**2)*(np.trace(Kz@Q) + alpha - 2*kappa.T@vz[0] + (vz@Q@vz.T)[0,0])
    
#    K0i_new = Kzi - np.dot(gamma, gamma.T)/(1 + (vz@np.asarray(gamma))[0])
    sd_new = sig2_delta + 1./n*(sig2_delta**2)*(np.trace(Kz@Q) + alpha - 2*kappa.T@vz + vz.T@Q@vz)[0,0]
    K0i_new = Kzi - np.dot(gamma, gamma.T)/(1 + vz.T@gamma)
    
    return({"Kz" : Kz, "vz" : vz, "sig2_delta" : sd_new, "K0i" : K0i_new})

def loglik_EM(vz, Kz, K0i, a):
    #Kzv = np.linalg.solve(Kz, vz.T)
    #ll = np.linalg.slogdet(K0i)[1] + np.linalg.slogdet(Kz)[1] + vz@Kzv - a
    #return(0.5*ll[0,0])
    Kzv = np.linalg.solve(Kz, vz)
    ll = np.linalg.slogdet(K0i)[1] + np.linalg.slogdet(Kz)[1] + vz.T@Kzv - a
    return(0.5*ll[0,0])

class organize_data:
    def __init__(self, 
                 dataset = "airs", 
                 variable = "TempK",
                 nres = 3, 
                 sample_prop = 1, 
                 res = 9):
        self.dataset = dataset # {airs, cris}
        self.nres = nres ## number of DGG basis function resolutions
        self.sample_prop = sample_prop ## proportion of each dataset to sample
        self.res = res ## DGG resolution of BAU grid        
        self.predlocs = None
        self.H = None
        
        if self.sample_prop > 1 or self.sample_prop <= 0:
            raise ValueError(f"sample_prop must be between 0 and 1.")

        if self.dataset not in ("airs","cris"):
            raise ValueError(f"invalid dataset name: {dataset}")
            
        #### set footprint size for dataset
        if dataset == "airs" :
            self.footprint = 45
        else :
            self.footprint = 42

        #%% Load Data (e.g. airs)
        dat = pd.read_csv("".join([dataset, "_data_all.csv"]))

        f = open("".join(["measurement_error_", dataset, ".txt"]),'r')
        self.error_sd = f.readline()
        f.close()  

        dat_coords = dat[['lon','lat']].values

        self.n = len(dat_coords)

        #### reorder data for splitting into train/test locations later
        
        if dataset == "airs" :
            random.seed(123)
        else :
            random.seed(456)

        # randomly reorder data
        rord = random.sample(range(self.n), self.n)

        self.latlon = dat_coords[rord,:]

        self.z = dat[[variable]].values[rord]
        #z = dat[['resid']].values
        
    def get_Hmatrix(self, BAU):
        
        #%%  Find overlap adjacency matrix
            
        bau_lon_sorted = np.argsort(BAU.predlocs[:,1])
        sort_grid = BAU.predlocs[bau_lon_sorted, :]  

        b_lat = (float(self.footprint) / 110) / 2
        b_long = (float(self.footprint) * (1/(abs(np.cos((self.latlon[:,1])*np.pi/180))*110))) / 2

        a_up = np.searchsorted(sort_grid[:, 1], self.latlon[:, 1] + b_lat)
        a_low = np.searchsorted(sort_grid[:, 1], self.latlon[:, 1] - b_lat)
        dat_ifoot = [np.where((y1 < sort_grid[x1:x2, 0]) & (sort_grid[x1:x2, 0] < y2))[0] 
                  for x1, x2, y1, y2 
                  in zip(a_low, a_up, self.latlon[:, 0] - b_long, self.latlon[:, 0] + b_long)]

        np_dat = np.asarray([len(x) for x in dat_ifoot])
        kp_dat = np_dat != 0.

        dat_ifoot = [dat_ifoot[x] for x in np.where(kp_dat)[0]]

        nfoot_dat = np.sum(kp_dat)
        iz = np.hstack([np.repeat(1.0/x, x) for x in np_dat[kp_dat]])
        irow = np.hstack([np.repeat(x, y) for x, y in zip(range(nfoot_dat), np_dat[kp_dat])])
        icol = np.hstack([bau_lon_sorted[np.arange(x1, x2 + 1, 1)[y]] for x1, x2, y in zip(a_low[kp_dat], a_up[kp_dat], dat_ifoot)])

        self.z = self.z[kp_dat]
        self.H = sp.sparse.csc_matrix((iz, (irow, icol)), shape=(nfoot_dat, BAU.kgrid))

        self.latlon = self.latlon[kp_dat,:]
        self.n = nfoot_dat

    def split_train_test(self, BAU):
        if self.H is None:
            self.get_Hmatrix(BAU)
            
        #### split in training/test
        n1 = int(np.floor(0.7*self.n))

        tr_id = list(range(0, n1))
        ts_id = list(range(n1, self.n))

        H_tr = self.H[tr_id,:]
        z_tr = self.z[tr_id]

        #### subsample training data if sample_prop < 1
        ## using seed from above
        if self.sample_prop < 1 :
            n2 = int(np.floor(self.sample_prop*n1))
            tr_sub = random.sample(range(n1), n2)
            H_tr = sp.sparse.csc_matrix(self.H[tr_sub,:])
            z_tr = self.z[tr_sub]
            n1 = n2

            
        self.H_tr = H_tr
        self.z_tr = z_tr 
        self.S_tr = H_tr @ BAU.S0
        self.latlon_tr = self.latlon[tr_id,:]
        self.sig2_eps = np.repeat(float(self.error_sd)**2, n1)

        self.H_ts = self.H[ts_id,:]
        self.z_ts = self.z[ts_id]
        self.S_ts = self.H_ts @ BAU.S0  
        self.latlon_ts = self.latlon[ts_id,:]
        
class build_BAU:
    def __init__(self,
                nres,
                res):
        
        self.nres=nres
        self.res = res
        
        #%%  Build BAU grid
        bf = basisFunction()
        lvls = range(1, self.nres+1)
        bf.basisCenters(levels = lvls, position = 'regularHex')

        gridGlobe = bauGrid(win = [-90, 90, -180, 180], res = self.res, type = 'hex') 

        gridGlobe.newGrid()

        self.kgrid = len(gridGlobe.locations)

        #### Build S0 matrix
        S0_m = bf.Snot_sp(win = gridGlobe.window, locations = gridGlobe.locations)  
        self.S0 = S0_m.S0

        self.predlocs = S0_m.predlocs
        self.r = self.S0.shape[1]  
            